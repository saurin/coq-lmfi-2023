(** * Course2 : Inductive types in Coq **)

Set Universe Polymorphism.


(** * Preliminaries

The Coq file supporting today's course is available at 

#<a 
href="https://gitlab.math.univ-paris-diderot.fr/saurin/coq-lmfi-2023/-/blob/main/Lectures/Course2.v">
https://gitlab.math.univ-paris-diderot.fr/saurin/coq-lmfi-2023/-/blob/main/Lectures/Course2.v</a>#.

#<a 
href="https://gitlab.math.univ-paris-diderot.fr/saurin/coq-lmfi-2023/-/blob/main/TP/TP2.v">
https://gitlab.math.univ-paris-diderot.fr/saurin/coq-lmfi-2023/-/blob/main/TP/TP2.v</a>#.

The aim of this second lecture is to understand how to define and use inductive types 
to program in Coq, but first, we will come back to exercise 3 of TP1, about church 
numerals. 

*)

(* *)

(** **** Exercise 3 : Church numerals

Encode in Coq the Church numerals, for which the number 
<<n>> is represented by <<λf.λx.(f (f (... (f x))))>> where 
<<f>> is applied <<n>> times.

More precisely, define (without using [nat] nor any other inductive type):

 - a type [church : Type]
 - two constant [zero] and [one] of type [church]
 - a function [church_succ] of type [church->church]
 - two functions [church_plus] and [church_mult] of type [church->church->church]
 - a function [church_power]
 - a test [church_iszero]

Also define two functions [nat2church : nat -> church] and [church2nat : church -> nat] 

*)

(* *)

(** Let us first analyze the type of Church numerals:
the statement of the exercise tells us that church is 
a function of two arguments, [f] and [x], which iterates 
[f] a certain number of time over [x]. 

Therefore [f] shall have a type of the form [X -> Y] 
and necessarily [X] must be the type of [x]. Moreover, 
since [f] can be iterated, the type of its codomain shall 
be the same as the type of its domain: [X]=[Y]. 
Moreover, we want this type to apply on any pair of arguments 
the first of which is iterable on the second, and we thus 
introduce a universal quantification (dependent product type)
ending up with [forall X, (X->X)->(X->X)] as a type of 
Church numerals.

*)

Definition church := forall X, (X->X)->(X->X).

(** in defining [zero, one, ...], it is not 
needed to give the type of each argument (and neither must 
we provide a name for the type argument [X]) 
when the type of the constant is given : *)

Definition zero : church := fun _ f x => x.
Definition one : church := fun _ f x => f x.

(** in fact there is another candidates for one: *)

Definition onebis : church := fun _ f => f.
Definition two : church := fun _ f x => f (f x).

Definition succ : church -> church :=
 fun n => fun _ f x => n _ f (f x).

Definition succ' : church -> church :=
 fun n => fun _ f x => f (n _ f x).


Compute succ one.

Definition church2nat (n:church) := n _ S 0.
Check church2nat.

Compute (church2nat (succ one)).


Compute church2nat zero.
Compute church2nat one.
Compute church2nat onebis.
Compute church2nat two.

Fixpoint nat2church (n:nat) :=
 match n with
 | O => zero
 | S m => succ (nat2church m)
 end.


Compute nat2church 3.
Compute church2nat (nat2church 100).

(** In the following, we will use the ability of Coq 
to not require that we actually provide the actual 
type when it can be inferred to lighten the definitions.

But to be fully informative and in order to understand 
what happens with church_power, we provide below the 
definition of those functions providing explicitly the 
type instanciations: *)

Definition church_plus (n m:church) : church :=
  fun _ f x => n _ f (m _ f x).


Compute church_plus one two.
Compute church2nat (church_plus (nat2church 13) (nat2church 10)).

Definition church_mult (n m:church) : church :=
 fun _ f =>  n _ (m _ f).

Compute church_mult one two.
Compute church2nat (church_mult (nat2church 13) (nat2church 10)).

Definition church_pow (n m:church) : church :=
 fun _ =>  m _ (n _).

Check church_pow. 
Print church_pow.

Compute church_pow two two.
Compute church2nat (church_pow (nat2church 2) (nat2church 5)).


Fixpoint is_zero_nat (n:nat) :=
 match n with
 | O => true
 | S m => false
 end.


(** To compute whether n is equal to zero of not, we 
pass two arguments to n. The first one will be iterated 
n times on the second one. 
- Therefore, if n is zero, the first function is not 
iterated and the second argument is returned directly, 
this second argument should be [true]. 
- Otherwise, if n is not zero, the function will be 
iterated at least one and we would like is_zero n to 
return false, therefore it is sufficient that the 
first argument of n is the the constant function 
always returning zero, independently of its argument: 
[fun _ => false]. *)


Definition is_zero (n : church) : bool :=
  n _ (fun _ => false) true.

Compute is_zero zero.
Compute is_zero one.

(** Explicit typing of the above functions: *)

Definition church_plus_explicit (n m:church) : church :=
  fun X f x => n X f (m X f x).


Definition church_mult_explicit (n m:church) : church :=
 fun X f =>  n X (m X f).

Definition church_pow_explicit (n m:church) : church :=
 fun X =>  m (X->X) (n X).

Definition is_zero_explicit (n : church) : bool :=
  n bool (fun (b:bool) => false) true.




(** how to compute the predecessor?

we want that, to [zero], it associates [zero]
and, to [fun f x => f (f ... x)], it associates
[fun f x => f ... x] *)

Definition church_pred (n:church) : church :=
 fun _ f x =>
   n _ (fun g h => h (g f)) (fun _ => x) (fun u => u).

(* Variable church_pred : church -> church.*)

Definition church_pred_explicit (n:church) : church :=
 fun X f x =>
   n ((X->X)->X) (fun g h => h (g f)) (fun _ => x) (fun u => u).

Compute church2nat (church_pred zero).
Compute church2nat (church_pred one).
Compute church2nat (church_pred (nat2church 13)).

(** Attention, donne [universe inconsistency] si les univers
    polymorphes ne sont pas activés. *)


 
Definition church_minus (n m : church) : church :=
  m _ church_pred n.

Print church_minus.

Compute church2nat (church_minus two two).
Compute church2nat (church_minus (nat2church 10) (nat2church 7)).




(** We first complete exercise 3 of TP1, providing missing definitions of the 
following terms:

- two constant [zero] and [one] of type [church]
- a function [church_succ] of type [church->church]
- two functions [church_plus] and [church_mult] of type [church->church->church]
- a function [church_power]
- a test [church_iszero]
- two functions [nat2church : nat -> church] and [church2nat : church -> nat] 
 converting between Coq inductive definition of nats and the Church encoding 
of natural numbers. 
*)

(* *)

(** ** Predecessor and subtraction

In addition, we define:
- [church_pred] of type [church -> church], which associates zero to zero and 
to any other church numeral, associates its predecessor 
*)

(** Using [church_pred], try and define a function [church_minus] of type 
[church -> church -> church] which computes the difference between to nats: 
it should return [zero] if the first argument is less or equal to the 
second one and the difference otherwise.

Analyze carefully why universe polymorphism is needed in this case! 
(try defining this first without setting universe polymorphism...)

*)

(* *)

(** ** Universe constraints


Some (ab)uses of Coq universes are rejected by the system, since they 
endanger the logical soundness. The reason is similar to 
Russel's paradox, it is known as the 
#<a href="https://coq.inria.fr/library/Coq.Logic.Hurkens.html">Hurkens' paradox</a># 
in type theory.

A concrete example of _universe inconsistency_ will be considered during 
the practical session in the last extension of the exercise dealing with 
Church numerals and Church arithmetic, when defining [church_minus]. 

Since [church] is [forall X, (X->X)->(X->X)] here we would 
like to form [church church] and have it equivalent to 
[(church->church)->(church->church)]. This amounts to replacing variable 
[X] (of a certain [Type_i]) by the whole [church] itself, but here 
[church] can only be in [Type_(i+1)] or more (try this typing yourself!). 
This [church church] application is hence not doable when universe 
levels are fixed at the time [church] is defined. A solution here 
with a modern Coq is to activate _universe polymorphism_ 
*)

Set Universe Polymorphism.

(**

and to let Coq pick universe levels when a definition is _used_, 
not when it is defined. This helps greatly in practice (but not always).

*)

(* *)


(** * Defining new Inductive types

Some inductive types are predefined in Coq, lots of them are defined 
in the standard library, but you can define your own inductive type and 
enrich the type system with those new definitions.

The keyword [Inductive] allows to enrich the system with a new type 
definition, expressed via several _constructor_ rules. The general 
syntax of a inductive type declaration is :

[[
Inductive t :=
| C₁ : A₁₁ -> ... -> A₁ₚ -> t
| ...
| Cₙ : Aₙ₁ -> ... -> Aₙₖ -> t
]]

The [Cᵢ] are _constructors_ of type [t], they may require some arguments 
(or not), but anyway they always have [t] as result type (after the 
rightmost arrow).

In fact, [t] itself may have arguments, turning it into an _inductive 
type scheme_ (we also say _inductive predicate_). We will see examples of 
that later.

Basic examples (already in the Coq standard library, no need to copy-paste 
them).

[[
Inductive unit : Set := tt : unit.

Inductive bool :=
| true : bool
| false : bool.

Inductive nat :=
| O : nat
| S : nat -> nat.
]]

*)

Inductive biznat :=
| O : nat
| S : nat → nat
|weird: (nat -> nat) -> nat.

Print unit. 
Print bool. 
Print nat. 



(** * Positivity constraints

Some inductive declarations are rejected by Coq, once again for preserving 
logical soundness. Roughly speaking, the inductive type being currently 
declared _cannot appear as argument of an argument of a constructor_ of 
this type. This condition is named _strict positivity_. 

As an example, the following inductive type definition is rejected by Coq: 
*)

Fail Inductive lam :=
|Fun : (lam -> lam) -> lam.

(**
On the other hand, one can consider such a type in Ocaml, let us illustrate 
the danger of this type, in OCaml:

- First, a "lambda-calcul" version:
[[
type lam = Fun : (lam -> lam) -> lam 
]]
 In the type of Fun, the leftmost 
 "lam" would be a non-positive occurrence in Coq.
[[
let identity = Fun (fun t -> t)
let app (Fun f) g = f g
let delta = Fun (fun x -> app x x)
let dd = app delta delta
]]
 [dd] allows for infinite evaluation, even without "let rec" !

- Second, a version producing an infinite computation in any type (hence 
 could be in Coq's False):
[[
type 'a poly = Poly : ('a poly -> 'a) -> 'a poly
let app (Poly f) g = f g
let delta = Poly (fun x -> app x x)
let dd : 'a = app delta delta
]]
*)

(** In Coq, this term [dd] would be a closed proof of [False] if these kinds of 
inductive types would be accepted. Once again, this is also closely related 
with the fact that Coq is strongly normalizing (no infinite computations).

*)

(* *)

(** * Match

The [match] operator (or _pattern-matching_) is a case analysis, following 
the different possible constructors of an inductive type.
It is very similar to OCaml's <<match>>, except for little syntactic 
differences ([=>] in "branches", final keyword [end]).

[[
match ... with
| C₁ x₁₁ ... x₁ₚ => ...
| ...
| Cₙ xₙ₁ ... xₙₖ => ...
end
]]

The _head_ of a match (what is between [match] and [with]) should be of the 
right inductive type, the one corresponding to constructors [C₁] ... [Cₙ].

Usually, the _branches_ (parts after [=>]) contains codes that have all the 
same type. We'll see later that this is not mandatory (see session on 
_dependent types_ ).

Computation and match : when the head of a match starts with a inductive 
constructor [Ci], a _iota-reduction_ is possible. It replaces the whole 
match with just the branch corresponding to constructor [Ci], and also 
replaces all variables [xi₁]...[xiₚ] by concrete arguments found in match 
head after [Ci].

Example:
*)

Compute
 match S (S O) with
 | O => O
 | S x => x
 end.


(** This will reduce to [S O] (i.e. number 1 with nice pretty-printing). 
This computation is actually the definition of [pred] (natural number 
predecessor) from TP0, applied to [S (S O)] i.e. number 2.


*)
(* *)
(** * Fix

The [Fixpoint] construction allows to create recursive functions in Coq. 
Beware, as mentionned earlier, some recursive functions are rejected by Coq, 
which only accepts _structurally decreasing recursive functions_.

The keyword [Fixpoint] is to be used in replacement of [Definition], see 
examples below and exercise to follow.

Actually, there is a more primitive notion called [fix], allowing to define 
an _internal_ recursive function, at any place of a code. And [Fixpoint] is 
just a [Definition] followed by a [fix]. More on that later, but anyway, 
favor [Fixpoint] over [fix] when possible, it's way more convenient. 

A [Fixpoint] or [fix] defines necessarily a function, with at least one 
(inductive) argument which is distinguished for a special role : the 
_decreasing argument_ or _guard_. Before accepting this function, Coq 
checks that each recursive call is made on a syntactic _strict subterm_ 
of this special argument. Roughly this means any subpart of it is obtained 
via a [match] on this argument (and no re-construction afterwards). 
Nowadays, Coq determines automatically which argument may serve as guard, 
but you can still specify it manually (syntax [{struct n}]).

Computation of a [Fixpoint] or [fix] : when the guard argument of a 
fixpoint starts with an inductive constructor [Ci], a reduction may occur 
(it is also called _iota-réduction_, just as for [match]). This reduction 
replaces the whole fixpoint with its body (what is after the [:=]), while 
changing as well in the body the name of the recursive function by the 
whole fixpoint (for preparing forthcoming iterations).

*)
(* *)
(** * Some usual inductive types

** nat : natural numbers represented as Peano integers
*)

Print nat.

(** ** Binary representation of numbers
*)

Require Import ZArith.
Print positive.
Print N.
Print Z.

(** Nota bene : the "detour" by a specific type [positive] for strictly 
positive numbers allows to ensure that these representations are canonical, 
both for [N] and for [Z]. In particular, there is only one encoding of zero 
in each of these types ([N0] in type [N], [Z0] in type [Z]).

** Coq pairs
*)

Print prod.

Definition fst {A B} (p:A*B) := match p with
 | (a,b) => a
 end.

Definition fst' {A B} (p:A*B) :=
 let '(a,b) := p in a.

(** ** A first example of dependent type 

Remember that unit is the inductive type with one constructor 
and a single element. *)


Fixpoint pow n : Type :=
 match n with
 | 0 => unit
 | S n => (nat * (pow n))%type
 end.


(** ** The option type 

The option type over a type A is a datatype providing the option to return
an element of A, or none. It is useful to treat cases where there are 
exceptional situations, typically some object being undefined.  
the option type is defined in Coq as an inductive type with 
two constructors, Some and None: *)

Print option.

(** ** The list type *)

Print list.

Require Import List.
Import ListNotations.

Check (3 :: 4 :: []).

Fixpoint length {A} (l : list A) :=
 match l with
 | [] => 0
 | x :: l => S (length l)
 end.


(** ** Trees in Coq

No predefined type of trees in Coq (unlike [list], [option], etc). 
Indeed, there are zillions of possible variants, depending on your 
precise need. Hence each user will have to define its own (which is 
not so difficult). For instance here is a version with nothing at leaves 
and a natural number on nodes.
*)

Inductive tree :=
| leaf
| node : nat -> tree -> tree -> tree.

(** **** Exercise 9: Binary trees with distinct internal and external nodes.

By taking inspiration from the definition of lists above, define an 
inductive type [iotree] depending on two types [I] and [O]
such that every internal node is labelled with an element of type I 
and every leaf is labelled with an element of type O. 
*)

(* *)

(** **** Exercise 10: Lists alternating elements of two types.

By taking inspiration from the definition of lists above, define an 
inductive type [ablists] depending on two types [A] and [B] which is 
constituted of lists of elements of types alternating between [A] and [B].
*)

(* *)


(*************************************************************************)
(*************************************************************************)
(*************************************************************************)
(*************************************************************************)




(** * Practice: TP 2

*)

(** ** Recursive definitions

**** Exercise 6 : Fibonacci

- Define a function [fib] such that [fib 0 = 0], [fib 1 = 1] 
 then [fib (n+2) = fib (n+1) + fib n].  (you may use a [as] 
 keyword to name some subpart of the [match] pattern ("motif" en français)).
- Define an optimized version of [fib] that computes faster that 
 the previous one by using Coq pairs.
- Same question with just natural numbers, no pairs. 
 Hint: use a special recursive style called "tail recursion".
- Load the library of binary numbers via [Require Import NArith].
 Adapt you previous functions for them now to have type [nat -> N].
 What impact does it have on efficiency ?
 Is it possible to simply obtain functions of type [N -> N] ?
*)

(* *)

(** **** Exercise 7 : Fibonacci though matrices

- Define a type of 2x2 matrices of numbers (for instance via a quadruple).
- Define the multiplication and the power of these matrices.
 Hint: the power may use an argument of type [positive].
- Define a fibonacci function through power of the following matrix:

<<
1 1
1 0
>>
*)

(* *)

(** **** Exercise 8 : Fibonacci decomposition of numbers

We aim here at programming the Zeckendorf theorem in practice : 
every number can be decomposed in a sum of Fibonacci numbers, and moreover 
this decomposition is unique as soon as these Fibonacci numbers are 
distinct and non-successive and with index at least 2.

Load the list library: *)

Require Import List.
Import ListNotations.

(**
- Write a function [fib_inv : nat -> nat] such that if 
 [fib_inv n = k] then [fib k <= n < fib (k+1)].
- Write a function [fib_sum : list nat -> nat] such that 
 [fib_sum [k_1;...;k_p] = fib k_1 + ... + fib k_p].
- Write a function [decomp : nat -> list nat] such that 
 [fib_sum (decomp n) = n] and [decomp n] does not contain 0 
 nor 1 nor any redundancy nor any successive numbers.
- (Optional) Write a function [normalise : list nat -> list nat] 
 which receives a decomposition without 0 nor 1 nor redundancy, 
 but may contains successive numbers, and builds a decomposition 
 without 0 nor 1 nor redundancy nor successive numbers. You might 
 assume here that the input list of this function is sorted in the 
 way you prefer. 

**)

(* *)

(** ** Some inductive types

**** Exercise 9: Binary trees with distinct internal and external nodes.

By taking inspiration from the definition of lists above, define an 
inductive type [iotree] depending on two types [I] and [O]
such that every internal node is labelled with an element of type I 
and every leaf is labelled with an element of type O. 
*)

(* *)

(** **** Exercise 10: Lists alternating elements of two types.

By taking inspiration from the definition of lists above, define an 
inductive type [ablists] depending on two types [A] and [B] which is 
constituted of lists of elements of types alternating between [A] and [B].
*)

(* *)

(** In the exercises below, you should have loaded the 
following Coq libraries: *)

Require Import Bool Arith List.
Import ListNotations.


(** **** Exercise 11: Classical exercises on lists

Program the following functions, without using the 
corresponding functions from the Coq standard library :

 - [length]
 - concatenate ([app] in Coq, infix notation [++])
 - [rev] (for reverse, a.k.a mirror)
 - [map : forall {A B}, (A->B)-> list A -> list B]
 - [filter : forall {A}, (A->bool) -> list A -> list A]
 - at least one [fold] function, either [fold_right] or [fold_left]
 - [seq : nat -> nat -> list nat], such that 
  [seq a n = [a; a+1; ... a+n-1]]

Why is it hard to program function like [head], [last] 
or [nth] ? How can we do ?
*)

(* *)
(** **** Exercise 12:  Some executable predicates on lists

 - [forallb : forall {A}, (A->bool) -> list A -> bool].
 - [increasing] which tests whether a list of numbers is 
  strictly increasing.
 - [delta] which tests whether two successive numbers of 
  the list are always apart by at least [k].
 
*)

(* *)
(** **** Exercise 13: Mergesort

- Write a [split] function which dispatch the elements of 
 a list into two lists of half the size (or almost). It is 
 not important whether an element ends in the first or second 
 list. In particular, concatenating the two final lists will 
 not necessary produce back the initial one, just a permutation 
 of it.
- Write a [merge] function which merges two sorted lists into a 
 new sorted list containing all elements of the initial ones. 
 This can be done with structural recursion thanks to a inner 
 [fix] (see [ack] in the session 3 of the course). 
 - Write a [mergesort] function based on the former functions.
*)

(* *)

(** **** Exercise 14: Powerset

Write a [powerset] function which takes a list [l] and returns 
the list of all subsets of [l].
For instance [powerset [1;2] = [[];[1];[2];[1;2]]]. 
The order of subsets in the produced list is not relevant.

*)
