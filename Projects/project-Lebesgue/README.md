# Lebesgue integration theory in Coq #

This project is quite complex and of a different kind than the other ones. 

The theory of Lebesgue integral has been formalized very recently in Coq by Boldo and her colleagues, but Lebesgue integral is currently only handled only for non-negative functions in their library. 

The project is twofold: 
- working and understanding the structure of the library that I would provide
- extending the formalization to general mesurable functions. 

The library is still quite experimental and quite large as well. This project is therefore quite complex. 

If you are interestes working on this topic, please contact me so that I can provide more details during a meeting / visio chat.



