Coq Project : Coherence of Heyting arithmetic
=============================================


## Presentation

The aim of the project is to prove in Coq the **coherence of
first-order Heyting arithmetic** (aka. HA). One reminds that HA is the FO intuitionistic logical theory built on the language of Peano arithmetic  and on Pean axioms.

## Aim

You shall fill the joint file [`Heyting.v`](Heyting.v).ø

It is not explicitely requred (but not forbidden either...) to hand a
short report, separated from the sources explaining your work. In any
case, the code should be *readable* (that is lines should not be too long, should be commented adequately and an explicit naming of hypothesis and variables in proof scripts will be appreciated.)

The development shall not use Coq standard library and no axiom 
(including those from the standard library: no use of classical logic!). You can use any recent version of Coq, but please mention the version you use in the start of your project.

It is highly recommended to read the whole subject before starting. 
As an information, everything can be treated in less that 1000 
lines (the starting file is already about 500 lines).

## Advices and useful notions

Consider using extensively the documentation available at <http://coq.inria.fr/>

### Arithmetic

One shall use here two comparison functions between natural numbers:

  - `x <=? y` (function `Nat.leb`) to test whether `x <= y`, providing a boolean result (of type `bool`, with value `true` or `false`).
  - `x ?= y` (function `Nat.compare`) to test whether `x=y` or `x<y` or `y<x`. the answr is of type `comparison` (values `Eq`, `Lt`, `Gt`).

Beware not to confuse `?=` (ternary comparison) and `=?` (boolean equality test).

When a goal uses a sub-term of the  form `if x <=? y then ... else ...`, a case analysis can be performed by using `destruct (Nat.leb_spec x y)`. This generates two sub-goals: the first one corresponding to case `x <= y`, and the second one  to case `y < x`.

In a goal using a sub-term of the form `match x ?= y with ...`, a case analysis can be performed using `destruct (Nat.compare_spec x y)`. This generates three sub-goals, corresponding to the three possible cases: `x = y`, `x < y` and `x > y`.

A tactique `break` is provided to do those operations automatically.

### Automation

Some fragments of arithmétic are decidable, and tactics allow to solve them automatically. In this project, one uses one of them, namely 
*Presburger arithmétic*.

Presburger arithmetic essentially corresponds to FO Peano arithmetic using only the addition. In Coq, `lia` allows to solve a good part of it. In particular, all goals using only additions and comparisons can be solved directly. This tactics is aware of associativity and commutativity of adition, transitivity of inequalities and compatibility between ddition and the inequalities as well.

### (Other) useful tactics

For more detailm consult Coq reference manual.

  - You can use  `auto` and `intuition`.

  - `f_equal` can be helpful to prove an equality of the form `f x ... = f y ...` by proving equality of each pair of arguments: `x=y`, etc.

  - `simpl` only unfolds definitions and computes. It happens
  than one wants to turn a term into another one that is convertible to it: use the `change` tactic.

  - `replace` allows to... replace a sub-term by another one if they are probably equal.

  - `set` and `pose` allow to name sub-terms (i.e. to do local definitions in a proof). They can help clarifying the display of a goal or simplify the arguments of certain tactics.

  - Lemmas about logical equivalence can be directly used with `apply`. For istance, if you have a lemma `truc:A<->B`, using `apply <- truc` or `apply -> truc` applies one direction of the equivalence. It is also possible to use directly the `rewrite` tactic (as for equality) when you have loaded the `Setoid` library.

  - Even though your final file should not use `admit`, it may be handy to use it to first focus on a subpart of a proof. Of course, in the end, you should have a few `Axiom`, `Parameter`, `admit` and `Admitted` left... the notation will depend on this obviously.

  - Command `Print Assumptions` allows to identify all the axioms used in the proof of a proposition.


### Methodology

You shall fill the attached file by completing the `(* TODO *)`. 
The rest of this file follows the structure of the attached file. 

All definitions and lemmas which are expected are explicitely mentioned and named in the rest of the present file, together ith numbered questions. 
TYhere is no need to prove things in order though. Still the order of statements shall not be changed. 

Of course, you can (and sometimes shall) use intermediate lemmas in addition of the subject (but at least all those from the subject shall be proved).

## Syntax

In this section, one defines the *object language* uner study, that is the notions of *terms*, *formulas* and of *derivations* (and thus, of theorem) in HA. These definitions are expressed in Coq, which serves as a  *meta language* and in which the properties of the logical system under study are expressed and proved.

### Terms and formulas

To simplify, one restricts ourselves to the language of Peano, even though a good prt of what follows could be done with more generality.

FO terms use free variables, as well as variables bound by quantifiers. The definition of substitution is quite delicate ue to variable capture, and the notion of alpha-equivalence was introduced to solve this issue. Although, the definition of substitution representing variables with names and manipulating explicitly alpha-equivalence, is difficult to use in Coq. That is why one shall use another term representation.

Bound variables will be represented with *De Bruijn indices*: a variable is coded by a number representing the number of quantifiers that one shall travers to reach the binding quantifier in the formula abstract syntact tree. Free variables are those which go to the top of the term. The type of terms is:

```coq
Inductive term :=
  | Tvar : nat -> term
  | Tzero : term
  | Tsucc : term -> term
  | Tplus : term -> term -> term
  | Tmult : term -> term -> term.
```

and that of formulas is:

```coq
Inductive formula :=
  | Fequal : term -> term -> formula
  | Ffalse : formula
  | Fand : formula -> formula -> formula
  | For : formula -> formula -> formula
  | Fimplies : formula -> formula -> formula
  | Fexists : formula -> formula
  | Fforall : formula -> formula.
```

With this encoding formula `∀n,∃p, n = 1 * p` is represented by:

```coq
Fforall (Fexists (Fequal (Tvar 1) (Tmult (Tsucc Tzero) (Tvar 0))))
```

This representation has the advantage that two alpha-equivalent formulas are syntactically equal. Still, the phenomenon of variable capture can still happen and one shall be careful. Luckily, this care will be quite systematic and treated quite uniformly by using functions `tlift` and `flift`, as well as a series of associated lemmas, given (and proved) in the model file. Look carefully at these results as you will have to use them in what follows.

### Closed expressions

With a representation with De Bruijn indeices, the usual notion of set of free variables shall be replaced with a bound on all free variables occurring in the expression. The predicate `cterm n t` corresponds to the fact tht all free variables of `t` are strictly less than `n`. Beware of the fact that quantifiers are taken into accound: when going below a quantifier, the bound is incremented by one. Predicates `cterm` and `cformula` are defined inductively in the model file.

**Question**: Prove the following properties:

```coq
Lemma cterm_1 : forall n t, cterm n t ->
  forall n', n <= n' -> cterm n' t.

Lemma cterm_2 : forall n t, cterm n t ->
  forall k, tlift k t n = t.

Lemma cterm_3 : forall n t, cterm n t ->
  forall t' j, n <= j -> tsubst j t' t = t.

Lemma cterm_4 : forall n t, cterm (S n) t ->
  forall t', cterm 0 t' -> cterm n (tsubst n t' t).
```

**Question**: Prove the corresponding properties for formulas.

### Natural deduction

A derivation in natural deduction of judgment `Γ ⊢ A` is represented in Coq by `rule Γ A` and written `Γ : A`. Predicata `rule` (provided in the file) is defined inductively with the usual rule of natural deduction, adapted to the use of De Bruijn indices.

**Question**: one omitted the language and inference rules for connectives `⊤` (`Ftrue`), negation (`Fnot`) and equivalence (`Fequiv`). Define them and prove that the corresponding introduction and elimination rules  are admissible.

**Question**: Define an operator `nFforall`, by iterating  `Fforall`. As an example, formule `nFforall 2 A` shall be convertible to `Fforall (Fforall A)`. Prove the following property:

```coq
Lemma nFforall_1 : forall n x t A,
  fsubst x t (nFforall n A) = nFforall n (fsubst (n + x) t A).
```

### Notations

In order to make formulas more readable, notations are introduced in the model file. These notations, which redefine a lot of existing notationns of Coq standard library, are defined in a differenet notation *scope*. So that these notations already existing have the usual interprtation (at the level of the meta language), but the introduced notations (at the level of the object language) can be used with notation `(...)%pa`: for instance, `A \/ B` represents metalevel conjunction, and `(A \/ B)%pa` represents  object-level conjunction (i.e. formula `Fand A B`).

### Theory

The Peano axioms are represented in the model file by the predicate `PeanoAx`:  proposition `PeanoAx P` means that  `P` is a Peano axiom. Look carefully how `PeanoAx` is defined. This definition allows to define in Coq the notion of *theorem*:

```coq
Definition Thm T := exists axioms,
  (forall A, In A axioms -> PeanoAx A) /\ axioms :- T.
```

**Question**: prove in HA the following formula: `∀n, n≠s(n)`. One
asks here an object-level proof: one shall express this formula as a  Coq term `A` of type `formula`, and then prove that `Thm A`.

## Semantics

The proof of coherence of HA that shall be studied here consists in building a model, with type `nat` as a support.

### Interpretation

A valuation of variables is represented by a list `b` of values (which are here all of type `nat`): variable number `i` is thus interpreted a element  `i` in the list (written `nth i b 0`). The model file defines the following functions:

```coq
tinterp : list nat -> term -> nat
finterp : list nat -> formula -> Prop
```

that interpret terms and formulas of the object language as natural numbers and propositions in Coq. An object-formula (of type `formula`) is said to be *valid* if its interpretation is provable in Coq.

**Question**: prove the following properties:

```coq
Lemma tinterp_1 : forall t v0 v1 v2,
  tinterp (v0++v1++v2) (tlift (length v1) t (length v0)) =
  tinterp (v0++v2) t.

Lemma tinterp_2 : forall t' t v1 v2,
  tinterp (v1 ++ v2) (tsubst (length v1) t' t) =
  tinterp (v1 ++ (tinterp v2 t') :: v2) t.
```

**Question**: prove the analogous properties for formulas.


### Correctness of the model

**Question**: prove that the rules of natural deduction are sound wrt. the interpretation of formulas:

```coq
Lemma ND_soundness : forall e A, ND e A ->
  forall b, (forall B, In B e -> finterp b B) -> finterp b A.
```

**Question**: Prove that the axioms of Peano are valide:

```coq
Lemma Ax_soundness : forall A, Ax A -> forall b, finterp b A.
```

**Question**: Deduce that `Ffalse` is not a theorem:

```coq
Theorem coherence : ~Thm Ffalse.
```
