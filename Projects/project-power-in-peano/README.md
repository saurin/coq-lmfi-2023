Power Function in Peano Arithmetic
==================================

Coq Project, PF2, M2 LFMI

## Preliminary Remark

This Coq project oes not include a Coq file to be completed, neither any preliminary organization into auxiliary lemmas already stated. 

The indications below are less numerous than for other projects: it is an intermeiate project between very structures projects an entirely personal ones, to start discovering how to organize a Coq development while keeping a leading direction. Please ask if you miss informations.


## Aim

The aim of the projects can be sumed up in few lines: write a Coq predicate `IsNthPowerOf : nat -> nat -> nat -> Prop` using only the language of Peano arithmetic, and then show in Coq that
`forall x y z, IsNthPowerOf x y z <-> x = y^z`.

One recall that the language of Peano arithmetic is a subset of Coq language, it is made of `O` `S` `+` `*`, the equality predicate `=` (on natural numbers), logical connectives `/\` `\/` `->` `~` `<->` as well as quantifiers `forall` and `exists` as long as on only quantify over numbers. And that is it...

It is recommended to define `IsNthPowerOf` by using several intermediate predicates, as long as each fragments remains in the required language. For instance, on can start with :

```coq
Definition Le x y := exists z, x+z = y.
```

Then do an equivalence proof between this predicate `Le` and the standard order `<=` of Coq over `nat`, which is defined as an `Inductive` predicate.


## Libraries

This project can use all the standard library, and any recent versin of Coq. No external library is allowed though. It is certainly recommended to load the following, at least :

```coq
Require Import Arith Lia
```

Otherwisem  notation `^` for the power function `Nat.pow` will not be available. Once `Arith` is loaded, doing `Search "^"` will instruct you on the available lemmas on Coq power function. The `lia` tactic will also be useful since it allows to solve sets of (in)equalities of Presburger arithmetic (that is the fragment of Peano containing only additions and subtractions together with multiplications by constants).

You will probably need a bit of number theory : divisors, primality... See `Nat.divide` and `Nat.gcd` and the corresponding lemmas. A priori, there is no definition of primality over `nat` in Coq stdlib, you may define your own as well as few basic results on it, or you may use the one that exists for `Z` in library `ZArith` and `Znumtheory`, and use conversion functions `Z.of_nat` and `Z.to_nat`. There is nothing such as the chinese remainder theorem (neither on `nat` nor on `Z`): if you want to use it, you'll have to prove it. 

## Indications

Here are some help to define `IsNthPowerOf` using two approaches. Your project will get validated if one methor at least is defined an proved in Coq.  You may also define both and compare them.

If you want to start thinking by yourself, come back to the following later.

Remark first that there is a weaker predicate than the one of the exercize, which is for `x` to be some power of `y`, no matter which one. Said otherwise, writing `IsPowerOf : nat -> nat -> Prop` such that `forall x y, IsPowerOf x y <-> exists z, x = y^z`. Of coursem on can find  `IsPowerOf` from `IsNthPowerOf` simply with an `exists`. But the converse is not trivial. That said, it may be good practice to implement `IsPowerOf` and prove it before the other one.

Second remark: in the particular case when `y` is prime, is it quite simple to write `IsPowerOf x y` in this special case,
by studying which number can divide this power `x` of a prime number `y`.
No obvious generalization to a cpomposite `y` though. This specific predicate `IsPrimePower`  will be useful in the first method below though.

#### First method : code a finite sequence in with some prime basis

By taking a large enough prime number `e`, one can encode a finite integer sequence `u_0` ...`u_n` as a single number `u_0 + u_1 * e + ... u_n * e^n` where the digits in basis `e` are the `u_i`. These digits can be obtained by divisions by powers of `e`, and use of modulo `e`. 
Here the sequence to encode is the sequence of powers of `y` that is `1`, `y`, `y^2`, until `y^z` which must be `x`.

For details, see (notations `Ax:` et `Ex:` mean `forall x,` et `exists x,`) :

https://stason.org/TULARC/self-growth/puzzles/331-logic-hofstadter-p.html

Beware that there are few imprecisions in this text: the definition for `PRIME` should exclude cases 0 and 1 for instance, and the resulting formula can be simplified further. For instance we can probably avoid using `LENGTH`. Note that this gives a solution for `IsPowerOf` for `y=10` only but generalizing to any `y` is immediate. 

Moving to  `IsNthPowerOf` may require to encode two sequence at once : powers of `y` and successive numbers `1`, `2`, ...`z`. in any case you will have to prove that there are arbitrary large prime numbers.


#### Second method : using the Chinese remainder theorem

Another approach consists in using the chinese remainder theorem. See the second part of the following link (again `Ax:` and `Ex:` mean `forall x,` and `exists x,`) :

https://www.math.uni-bielefeld.de/~sillke/PUZZLES/power10

In particular, the very end offers an possible implementation of `IsNthPowerOf`. Here again, there may be some little adjustments to do here and there, but that is a good guiding principle. For instance the one-better-last line read `Aj: (0 < j <= w -> ...` 
Also, the lines starting with `:` in the midle part of this file may be skipped, at least at first read.


## Possible (optional) extensions

#### Write other recursive functions in Peano arithmetic

Using the previous idea, it is possible to represent in Peano arithmetic a whole bunch of recursive functions.
You may code for instance the relation corresponding to the factorial and prove it correct. Note that this may require using you predicate `IsNthPowerOf`!


#### Connection with the project on the coherence of Heyting arithmetic

In the project on Heyting artihmetic, an encoding in Coq for Heyting arithmetic is given. This is a "deep embedding" as terms and formulas are defined by specific data structures in Coq (via `Inductive` types). Define your predicate `IsNthPowerOf` as a formula `F` of type `formula`  given in file `Heyting.v`. This formula shall have 3 free variables  `Tvar 0` `Tvar 1` and `Tvar 2` corresponding respectively to `x` `y` and `z`. Check that `fun x y z => finterp [x;y;z] F` actually given a predicate that is convertible to `IsNthPowerOf`.

