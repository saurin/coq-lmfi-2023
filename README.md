# Programmation fonctionnelle et preuves formelles en Coq -- LMFI 2023
# Functional Programming and Formal Proofs using Coq -- LMFI 2023


You will find here course material for the course entitled "Functional Programming and Formal Proofs using Coq" of LMFI second-year master, academic year 2023-2024.
