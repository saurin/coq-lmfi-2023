(** * TP1 : Introduction to Functional Programming in Coq

** Functions and only that


**** Exercise 1 : Function composition

Define a function [compose : forall A B C, (B->C)->(A->B)->(A->C)], 
using the section mechanism seen in class.
Test it with functions [S] and [pred] on natural numbers (type [nat]).


*)

(* *)

(** **** Exercise 2 : Boolean ersatz

Define (without using [bool] nor any other inductive type):

- a type [mybool : Type] (Hint: think of the way definition 
of the identity function of type [forall X, (X -> X)]) (difficulty: medium);
- two constants [mytrue] and [myfalse] of type [mybool] (difficulty: easy);
- a function [myif : forall A, mybool -> A -> A -> A] such that 
 [myif mytrue x y] computes to [x] and [myif myfalse x y] computes to [y] 
(difficulty: easy).

*)

(* *)

(** **** Exercise 3 : Church numerals

Encode in Coq the Church numerals, for which the number 
<<n>> is represented by <<λf.λx.(f (f (... (f x))))>> where 
<<f>> is applied <<n>> times.

More precisely, define (without using [nat] nor any other inductive type):

- a type [church : Type] (Hint: (i) first rephrase the Church numerals 
 described above as Coq function definitions, in particular for zero 
 and one: as a function of two arguments, f and x, it should have a 
 type of the form [A -> B -> C], (ii) think about the constraints on 
 A, B and C, given by the representation of zero and one and (iii) 
 use a quantification as for mybool to obtain a closed type 
 (difficulty: medium);
- two constant [zero] and [one] of type [church] (difficulty: easy);
- a function [church_succ] of type [church->church] (difficulty: easy);
- two functions [church_plus] and [church_mult] of type 
 [church->church->church] (difficulty: medium);
- a function [church_power] (difficulty: hard);
- a test [church_iszero] (difficulty: hard);

Also define two functions [nat2church : nat -> church] and 
[church2nat : church -> nat] (difficulty: medium).

*)

(* *)

(** ** Base types

**** Exercise 4 : Booleans

- Write a function [checktauto : (bool->bool)->bool] which tests whether 
 a Boolean unary function always answers [true] (difficulty: easy).
- Same for [checktauto2] and [checktauto3] for Boolean functions expecting 2, 
 then 3 arguments. This can be done by enumerating all cases, but there is a 
 clever way to proceed (for instance by re-using [checktauto]
 (difficulty: medium).
- Check whether [fun a b c => a || b || c || negb (a && b) || negb (a && c)] is 
 a tautology (difficulty: easy).
 Note : the command [Open Scope bool_scope.] activates notations [||] and [&&] 
 (respectively for functions [orb] and [andb]).
- Define some functions behaving like Coq standard functions [negb] and [orb] 
 and [andb] (difficulty: easy).

*)

(* *)

(** **** Exercise 5 : Usual functions on natural numbers.

Define the following functions of type [nat  -> nat -> nat] 
(without using the ones of Coq standard library of course!):

- [addition] (difficulty: easy)
- [multiplication] (difficulty: easy)
- [subtraction] (difficulty: medium)
- [power]  (difficulty: medium)
- [modulo] such that [modulo a b] computes a modulo b 
 (difficulty: medium)
- [gcd] (difficulty: hard)

Ackermann-Péter (difficulty: hard):
We recall that the Ackermann-Péter function, AP, is defined as: 
- AP(0,n) = n+1
- AP(m+1, 0) = AP(m, 1)
- AP(m+1, n+1) = AP(m, AP(m+1, n)).

Try defining [AP], of type [nat -> nat -> nat], in the 
most natural way, based on its definition. 
What problem do you encounter?

What possible workaround can you imagine?

*)

(* *)

(** ** Recursive definitions

**** Exercise 6 : Fibonacci

- Define a function [fib] such that [fib 0 = 0], [fib 1 = 1] 
 then [fib (n+2) = fib (n+1) + fib n].  (you may use a [as] 
 keyword to name some subpart of the [match] pattern ("motif" en français)).
- Define an optimized version of [fib] that computes faster that 
 the previous one by using Coq pairs.
- Same question with just natural numbers, no pairs. 
 Hint: use a special recursive style called "tail recursion".
- Load the library of binary numbers via [Require Import NArith].
 Adapt you previous functions for them now to have type [nat -> N].
 What impact does it have on efficiency ?
 Is it possible to simply obtain functions of type [N -> N] ?
*)

(* *)

(** **** Exercise 7 : Fibonacci though matrices

- Define a type of 2x2 matrices of numbers (for instance via a quadruple).
- Define the multiplication and the power of these matrices.
 Hint: the power may use an argument of type [positive].
- Define a fibonacci function through power of the following matrix:

<<
1 1
1 0
>>
*)

(* *)

(** **** Exercise 8 : Fibonacci decomposition of numbers

We aim here at programming the Zeckendorf theorem in practice : 
every number can be decomposed in a sum of Fibonacci numbers, and moreover 
this decomposition is unique as soon as these Fibonacci numbers are 
distinct and non-successive and with index at least 2.

Load the list library: *)

Require Import List.
Import ListNotations.

(**
- Write a function [fib_inv : nat -> nat] such that if 
 [fib_inv n = k] then [fib k <= n < fib (k+1)].
- Write a function [fib_sum : list nat -> nat] such that 
 [fib_sum [k_1;...;k_p] = fib k_1 + ... + fib k_p].
- Write a function [decomp : nat -> list nat] such that 
 [fib_sum (decomp n) = n] and [decomp n] does not contain 0 
 nor 1 nor any redundancy nor any successive numbers.
- (Optional) Write a function [normalise : list nat -> list nat] 
 which receives a decomposition without 0 nor 1 nor redundancy, 
 but may contains successive numbers, and builds a decomposition 
 without 0 nor 1 nor redundancy nor successive numbers. You might 
 assume here that the input list of this function is sorted in the 
 way you prefer. 

**)

(* *)

(** ** Some inductive types

**** Exercise 9: Binary trees with distinct internal and external nodes.

By taking inspiration from the definition of lists above, define an 
inductive type [iotree] depending on two types [I] and [O]
such that every internal node is labelled with an element of type I 
and every leaf is labelled with an element of type O. 
*)

(* *)

(** **** Exercise 10: Lists alternating elements of two types.

By taking inspiration from the definition of lists above, define an 
inductive type [ablists] depending on two types [A] and [B] which is 
constituted of lists of elements of types alternating between [A] and [B].
*)

