(** * TP2 : Inductive types in Coq **)


(** * Universe constraints *)


(** **** Exercise 3' : A follow-up to TP1 exercises on Church numerals

Recall that we can encode in Coq the Church numerals, for which the number 
<<n>> is represented by <<λf.λx.(f (f (... (f x))))>> where 
<<f>> is applied <<n>> times and therefore define a type [church] to 
represent those numbers and some arithmetical functions on this type:

*)

Definition church := forall X : Type, (X -> X) -> X -> X.


(**

Define a function [church_pred] of type [church -> church], which associates zero to zero and to any other church numeral, associates its predecessor 

Using [church_pred], try and define a function [church_minus] of type 
[church -> church -> church] which computes the difference between to nats: 
it should return [zero] if the first argument is less or equal to the 
second one and the difference otherwise.

Analyze carefully why universe polymorphism is needed in this case! 
(try defining this first without setting universe polymorphism...)

Then, redefine [church_pred] after setting:
*)

Set Universe Polymorphism.

(* *)


(** ** Recursive definitions

**** Exercise 6 : Fibonacci

- Define a function [fib] such that [fib 0 = 0], [fib 1 = 1] 
 then [fib (n+2) = fib (n+1) + fib n].  (you may use a [as] 
 keyword to name some subpart of the [match] pattern ("motif" en français)).
- Define an optimized version of [fib] that computes faster that 
 the previous one by using Coq pairs.
- Same question with just natural numbers, no pairs. 
 Hint: use a special recursive style called "tail recursion".
- Load the library of binary numbers via [Require Import NArith].
 Adapt you previous functions for them now to have type [nat -> N].
 What impact does it have on efficiency ?
 Is it possible to simply obtain functions of type [N -> N] ?
*)

(* *)

(** **** Exercise 7 : Fibonacci though matrices

- Define a type of 2x2 matrices of numbers (for instance via a quadruple).
- Define the multiplication and the power of these matrices.
 Hint: the power may use an argument of type [positive].
- Define a fibonacci function through power of the following matrix:

<<
1 1
1 0
>>
*)

(* *)

(** **** Exercise 8 : Fibonacci decomposition of numbers

We aim here at programming the Zeckendorf theorem in practice : 
every number can be decomposed in a sum of Fibonacci numbers, and moreover 
this decomposition is unique as soon as these Fibonacci numbers are 
distinct and non-successive and with index at least 2.

Load the list library: *)

Require Import List.
Import ListNotations.

(**
- Write a function [fib_inv : nat -> nat] such that if 
 [fib_inv n = k] then [fib k <= n < fib (k+1)].
- Write a function [fib_sum : list nat -> nat] such that 
 [fib_sum [k_1;...;k_p] = fib k_1 + ... + fib k_p].
- Write a function [decomp : nat -> list nat] such that 
 [fib_sum (decomp n) = n] and [decomp n] does not contain 0 
 nor 1 nor any redundancy nor any successive numbers.
- (Optional) Write a function [normalise : list nat -> list nat] 
 which receives a decomposition without 0 nor 1 nor redundancy, 
 but may contains successive numbers, and builds a decomposition 
 without 0 nor 1 nor redundancy nor successive numbers. You might 
 assume here that the input list of this function is sorted in the 
 way you prefer. 

**)

(* *)

(** ** Some inductive types

**** Exercise 9: Binary trees with distinct internal and external nodes.

By taking inspiration from the definition of lists above, define an 
inductive type [iotree] depending on two types [I] and [O]
such that every internal node is labelled with an element of type I 
and every leaf is labelled with an element of type O. 
*)

(* *)

(** **** Exercise 10: Lists alternating elements of two types.

By taking inspiration from the definition of lists above, define an 
inductive type [ablists] depending on two types [A] and [B] which is 
constituted of lists of elements of types alternating between [A] and [B].
*)

(* *)

(** In the exercises below, you should have loaded the 
following Coq libraries: *)

Require Import Bool Arith List.
Import ListNotations.


(** **** Exercise 11: Classical exercises on lists

Program the following functions, without using the 
corresponding functions from the Coq standard library :

 - [length]
 - concatenate ([app] in Coq, infix notation [++])
 - [rev] (for reverse, a.k.a mirror)
 - [map : forall {A B}, (A->B)-> list A -> list B]
 - [filter : forall {A}, (A->bool) -> list A -> list A]
 - at least one [fold] function, either [fold_right : 
 forall A B : Type, (B -> A -> A) -> A -> list B -> A] or 
 [fold_left: forall A B : Type, (A -> B -> A) -> list B -> A -> A]. 
Their behaviour is defined as: 
[fold_right f a [b1; ...; bk] = f b1 (f b2 (... (f bk a)...))]
and 
[fold_left f a [b1; ...; bk] = f (... (f (f a b1) b2 )...) bk)]
The fold functions take an 
operation a function with inputs in 
A and B that returns an element in A, an starting element of 
type A and a list of element of type B and 
 - [seq : nat -> nat -> list nat], such that 
  [seq a n = [a; a+1; ... a+n-1]]

Why is it hard to program function like [head], [last] 
or [nth] ? How can we do ?
*)

(* *)
(** **** Exercise 12:  Some executable predicates on lists

 - [forallb : forall {A}, (A->bool) -> list A -> bool].
 - [increasing] which tests whether a list of numbers is 
  strictly increasing.
 - [delta] which tests whether two successive numbers of 
  the list are always apart by at least [k].
 
*)

(* *)
(** **** Exercise 13: Mergesort

- Write a [split] function which dispatch the elements of 
 a list into two lists of half the size (or almost). It is 
 not important whether an element ends in the first or second 
 list. In particular, concatenating the two final lists will 
 not necessary produce back the initial one, just a permutation 
 of it.
- Write a [merge] function which merges two sorted lists into a 
 new sorted list containing all elements of the initial ones. 
 This can be done with structural recursion thanks to a inner 
 [fix] (see [ack] in the session 3 of the course). 
 - Write a [mergesort] function based on the former functions.
*)

(* *)

(** **** Exercise 14: Powerset

Write a [powerset] function which takes a list [l] and returns 
the list of all subsets of [l].
For instance [powerset [1;2] = [[];[1];[2];[1;2]]]. 
The order of subsets in the produced list is not relevant.

*)
