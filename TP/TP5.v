(** * TP5: First proofs in Coq *)

(** In the following exercises, you will 
write your first proofs in Coq. 

The aim is to get familiar with the first tactics proving 
some logical statements, in propositional logic first and
then in first-order logic. 

Next, you will start formalizing yourself your first 
statements and proving them. *)

(*   Check 0.
  Check S.
  Check nat.
  Print nat.
  Search nat.
  Check 2 + 2 = 5.
  Check forall x, exists y, x = 2 * y \/ x = 2 * y + 1.
  Definition id := fun (A : Set) (x : A) => x.
  Check id.
  Check id nat 7.
*)

(** **** Exercise 1 : Propositional calculus 

Assume some propositional variables [A] and [B] and [C] 
via the command:
*)

Section PropositionalLogic.

Parameters A B C : Prop.

(** Then prove in Coq the following statements (that is, 
you have to replace [Admitted.] by a series of tactics that
completely "solve" the goal before ending your proof with [Qed.]): *)

Lemma E1F1 : A -> A.
Proof. 
(* Complete here *)
Admitted.

Lemma E1F2 : (A -> B) -> (B -> C) -> A -> C.
Proof. 
(* Complete here *)
Admitted.

Lemma E1F3 : A /\ B <-> B /\ A.
Proof. 
(* Complete here *)
Admitted.

Lemma E1F4 : A \/ B <-> B \/ A.
Proof. 
(* Complete here *)
Admitted.

Lemma E1F5 : (A /\ B) /\ C <-> A /\ (B /\ C).
Proof. 
(* Complete here *)
Admitted.

Lemma E1F6 : (A \/ B) \/ C <-> A \/ (B \/ C).
Proof. 
(* Complete here *)
Admitted.

Lemma E1F7 : A -> ~~A.
Proof. 
(* Complete here *)
Admitted.

Lemma E1F8 : (A -> B) -> ~B -> ~A.
Proof. 
(* Complete here *)
Admitted.

Lemma E1F9 : ~~(A \/ ~A).
Proof. 
(* Complete here *)
Admitted.

End PropositionalLogic.

(** **** Exercise 2 : Predicate calculus 

First perform the following declarations :
*)

Section PredicateCalculus. 

Parameter X Y : Set.
Parameter P Q : X -> Prop.
Parameter T : X -> Y -> Prop.


(**
Then prove in Coq the following statements :
*)

Lemma E2F1 : 
(forall x, P x /\ Q x) <-> (forall x, P x) /\ (forall x, Q x).
Proof. 
(* Complete here *)
Admitted.

Lemma E2F2 : 
(exists x, P x \/ Q x) <-> (exists x, P x) \/ (exists x, Q x).
Proof. 
(* Complete here *)
Admitted.

Lemma E2F3 : 
(exists y, forall x, T x y) -> forall x, exists y, T x y.
Proof. 
(* Complete here *)
Admitted.

End PredicateCalculus. 


(** **** Exercise 3 : Order relations 

Let us consider a type [E:Type] equipped by a binary relation 
[R] assumed to satisfy the axioms of order relations:
*)

Section OrderRel.

Parameter E : Type.
Parameter R : E -> E -> Prop.
Axiom refl : forall x : E, R x x.
Axiom trans : forall x y z : E, R x y -> R y z -> R x z.
Axiom antisym : forall x y : E, R x y -> R y x -> x = y.

(**
We define the notion of smallest and minimal elements this way:
*)


Definition smallest (x0 : E) := forall x : E, R x0 x.
Definition minimal (x0 : E) := forall x : E, R x x0 -> x = x0.

(**

- What are the types of [smallest] and [minimal] ?

- State in Coq and prove the following lemmas:

  - If [R] admits a smallest element, this one is unique.
  - The smallest element, if it exists, is a minimal element.
  - If [R] admits a smallest element, then there is no 
   other minimal element that this one.
*)

End OrderRel.

(* *)

(** *** Exercice 4 : Classical logic 

In this exercise, we assume the reasoning rule "by absurdum", 
that we can declare in Coq via:
*)

Section Classical. 

Axiom not_not_elim : forall A : Prop, ~~A -> A.

(**
- Show in Coq that this axiom implies the excluded-middle : 
 [forall A : Prop, A \/ ~ A].

- We now aim at proving the Drinker's paradox (due to Smullyan):

  - In an non-empty room we can find someone with the following 
   property: if this person drinks, then everybody in the room drink.

 Declare in Coq the needed elements to formalize the 
 problem (cf. the previous exercise).
  
- State and prove this paradox (thanks to excluded-middle).
*)

End Classical. 

(* *) 
(** *** Exercice 5 : Subsets 
*)

Section Subsets.

(** Given a type [E:Type] in Coq, we consider subsets of [E], 
represented here as unary predicates on [E], i.e. objects 
of type [E->Prop].
  
  1. Define in Coq the binary predicate 
[subset : (E->Prop)->(E->Prop)->Prop] expressing the inclusion of 
two subsets. Show that this relation is reflexive and transitive. 
Is is antisymmetric ?

  2. Define in Coq a binary predicate 
[eq : (E->Prop)->(E->Prop)->Prop] expressing the extensional 
equality of two subsets. Show that it is indeed a equivalence 
relation. Show that [subset] is antisymmetric with respect of [eq].

  3. Define in Coq the union and intersection operators on 
subsets of [E]. Show that theses operations are associative, 
commutative, idempotent and distributive (one on the other).
*)

End Subsets.


(** *** Exercise 6 : Formalizing

State in Coq and prove some of the questions in this 
#<a href="https://gitlab.math.univ-paris-diderot.fr/letouzey/cours-preuves/blob/master/td1.pdf">https://gitlab.math.univ-paris-diderot.fr/letouzey/cours-preuves/blob/master/td1.pdf</a>.#
 
For simulating the reasoning "by absurdum", 
you can declare the following axiom:
*)





