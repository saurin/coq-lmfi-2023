(** * TP6 : Arithmetical Proofs in Coq *)

(** * Formatting

<<
coqdoc --html TP6.v  -o TP6.html --no-index --no-lib-name  --lib-subtitles --utf8 --parse-comments
>>
*)

(** * Revision : the inductive type of natural numbers

In Coq, a new data type can be introduced via the 
mecanism of _inductive definition_ (very similar 
to the algebraic types in OCaml). For instance, the 
type [nat] of natural numbers is introduced in the 
Coq standard library (cf. file 
[Init/Datatypes.v](https://coq.inria.fr/stdlib/Coq.Init.Datatypes.html)) 
via the following inductive definition:
*)

(* Inductive nat : Set :=
| O : nat
| S : nat -> nat.
*)

(**
This definition adds to the current environment three new constants:
  - the type [nat : Set] (where [Set] is an alias for the 
 lowest of [Type] universes);
  - the constructor [O : nat]
  - the constructor [S : nat -> nat]
Beware, the constructor for zero is [O] internally (the letter O), 
even if the system also accepts later numerical notations : [0] for 
[O], [1] for [(S O)], [2] for [(S (S O))], etc.
*)

(* *)

(** * Induction principles 

The former inductive definition also generates automatically a few 
induction principles. In practice, the mostly used is :


<<
nat_ind :
  forall P : nat -> Prop,
    P 0 -> (forall n : nat, P n -> P (S n)) -> forall n : nat, P n
>>

which is used internally by the [induction] tactic.

Other handy tactics on such an inductive type :
 - [simpl] : applies computation rules.
 - [rewrite H] : rewrite a term t with a term u when [H] has type [t=u], variant: [rewrite <- H].
 - [injection] : all inductive constructors are injective, from 
[H : S x = S y] then [injection H] provides [x = y].
 - [discriminate] : all inductive constructors are orthogonal, 
from [H : S x = O] then [discriminate H] or just [discriminate] 
proves [False] (hence anything).
 - [f_equal] : proves a goal [f x = f y], as long as sub-goal 
[x = y] could then be proved. Sort of dual to [injection], 
except that it works for any function [f], not only for 
inductive constructors.
*)

(* *)

(** *** Exercise 1 : Addition 

In Coq, the addition of natural numbers is defined 
via a [Fixpoint] (similar to the [let rec] of OCaml). 
See [Init/Nat.v](https://coq.inria.fr/stdlib/Coq.Init.Nat.html) :
*)

Fixpoint add (n m:nat) : nat :=
  match n with
   | O => m
   | S p => S (add p m)
  end.

(**
Note that the recursive call is done here on a first 
argument [p] which is stricly lower than [n]. Only 
such a *structural decrease* is allowed, Coq will refuse 
the definitions where it cannot verify this property, in 
order to avoid non-terminating computations.

The system uses the notation [n + m] to shorten the term 
[Nat.add n m].

Show the following lemmas on addtion (basic equalities, 
then associativity and commutativity). Which egalities 
are _"definitional"_ (obtained by mere computation and 
reflexivity, tactics [simpl] and [reflexivity]) ? 
For the other equalities, proceed by induction over [n], 
thanks to the [induction] tactic.
*)

Lemma add_0_l : forall n, 0 + n = n.
Proof.
Admitted.

Lemma add_succ_l : forall n m, S n + m = S (n + m).
Proof.
Admitted.

Lemma add_0_r : forall n, n + 0 = n.
Proof.
Admitted.

Lemma add_succ_r : forall n m, n + S m = S (n + m).
Proof.
Admitted.

Lemma add_assoc : forall n m p, (n + m) + p = n + (m + p).
Proof.
Admitted.

Lemma add_comm : forall n m, n + m = m + n.
Proof.
Admitted.

(** Prove the following lemma without using induction,
just the previous lemma and appropriate rewrites.
*)

Lemma add_permute : forall n m p, n+m+p = n+p+m.
Proof.
Admitted. 


(** *** Exercise 2 : Multiplication 

In Coq, the multiplication is defined by :
*)

Fixpoint mul (n m:nat) : nat :=
  match n with
   | O => O
   | S p => m + mul p m
  end.

(**
The system uses the notation [n * m] to shorten [mul n m].

Just as for addition, prove the following lemmas:
*)

Lemma mul_0_l : forall n, 0 * n = 0.
Proof.
Admitted.

Lemma mul_succ_l : forall n m, S n * m = m + n * m.
Proof.
Admitted.

Lemma mul_0_r : forall n, n * 0 = 0.
Proof.
Admitted.

Lemma mul_succ_r : forall n m, n * S m = n + n * m.
Proof.
Admitted.

Lemma mul_distr : forall n m p, (n + m) * p = n * p + m * p.
Proof.
Admitted.

Lemma mul_assoc : forall n m p, (n * m) * p = n * (m * p).
Proof.
Admitted.

Lemma mul_comm : forall n m, n * m = m * n.
Proof.
Admitted.



(** *** Exercise 3 : an order relation on numbers

Here is one possible way to define the large inequality 
on [nat] numbers:
*)

Definition le (n m : nat) := exists p, n + p = m.
Infix "<=" := le.

(**
Show that this predicate [le] is indeed an order relation:
*)

Lemma le_refl : forall n, n <= n.
Proof.
Admitted.

Lemma le_trans : forall n m p, n <= m -> m <= p -> n <= p.
Proof.
Admitted.

Lemma le_antisym : forall n m, n <= m -> m <= n -> n = m.
Proof.
Admitted.


(**
Also show the following statements:
*)

Lemma le_succ : forall n m, n <= m -> n <= S m.
Proof.
Admitted.

Lemma le_total : forall n m, n <= m \/ m <= n.
Proof.
Admitted.

(**
Note : this [le] definition is not the one used in the Coq standard library 
(cf. [Init/Peano.v](https://coq.inria.fr/stdlib/Coq.Init.Peano.html)), 
which is based on an inductive predicate. But these two definitions 
are equivalent, you could even try to already prove that. 
Hint : a proof using a [<=] of Coq (internal name [Peano.le]) 
can be done by induction over this hypothesis.
*)

(** *** Exercise 4: Back to school with Gauss

Define [sum_n : nat -> nat] such that [sum_n k] 
  is the sum of natural numbers from 1 to k.

Prove the following theorems: 
*)

Theorem sum_closed_form : forall k:nat, 2 * sum_n k = k * S k.
Proof. 
Admitted.

Theorem sum_k_le_k : forall k:nat, k <= sum_n k.
Proof. 
Admitted. 
  
(** *** Exercise 5: Being careful when destructing booleans

Prove the following lemma. Note that you will have to be careful about what you destruct!

Think of the informal proof before you start destructing everything.
*)

Lemma careful-destruct : forall (b: bool), forall (f: bool -> bool),
f (f (f b)) = f b.
Proof. 
Admitted.



(** *** Exercise 6: Even or Odd *)

Section Even_odd.

Inductive Even : nat -> Prop :=
    | even_0 : even 0
    | even_ss : forall x, even x -> even (S (S x)).

  Inductive Odd : nat -> Prop := 
    | odd_1 : Odd 1
    | odd_ss : forall x, Odd x -> Odd (S (S x)).

Lemma ex_even4: Even 4.
Proof.
Admitted.


Theorem S_even_is_odd : forall x, Even x -> Odd (S x).
  Proof.
Admitted.

Theorem S_odd_is_even : forall x, Odd x -> Even (S x).
  Proof.
Admitted.

Theorem Nat_even_or_odd : forall x, Even x \/ Odd x.
  Proof.
Admitted.

End Even_odd.

(** *** Exercise 7: Predicates on words *)

Section Words.

Variable char : Set.
Variable a : char.
Variable b : char.

Require Import List.

Definition word : Set := list char.

Definition pempty (w:word) : Prop := w = nil.

Definition pchar (c:char) (w:word) : Prop := w = cons c nil.

Definition pconcat (p1 p2 : word -> Prop) (w : word) : Prop :=
exists w1 w2, p1 w1 /\ p2 w2 /\ w = w1++w2.  

Theorem pconcat_empty : forall p w, pconcat p pempty w -> p w.
Proof.
Admitted.

Inductive pstar (p : word -> Prop) : word -> Prop :=
   | star_eps : pstar p nil
   | star_plus : forall w1 w2: word, p w1 -> pstar p w2 -> pstar p (w1++w2).

Theorem concat_star :
    forall (p : word -> Prop) w1 w2, p w1 -> pstar p w2 -> pstar p (app w1 w2).
  Proof.
Admitted.

Theorem star_concat :
    forall (p : word -> Prop) w1 w2, pstar p w1 -> p w2 -> pstar p (app w1 w2).
  Proof.
Admitted.

  Theorem star_pconcat :
    forall (p : word -> Prop) (w : word), pconcat (pstar p) p w -> pstar p w.
  Proof.
Admitted.

End Words.



(** * Epilogue 

When launching Coq, the definition of [nat] numbers is directly 
available, as well as the previous operations in *qualified* 
version, for instance [Nat.add] for [+]. Some older names are also 
available : [plus], [mult] but they are considered deprecated. 
The proofs about [nat] numbers and their operations are not available 
initially, they should be loaded via [Require Import Arith]. This 
command provides for instance [Nat.add_assoc] and many other proofs, 
including many lemmas of this TP. The [Search] command allows to 
search through all these available lemmas. Moreover, some other 
powerful tactics may help later, for instance [auto], [lia] (formerly 
[omega]) or [ring].
*)
