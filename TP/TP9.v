Require Export ZArith.
Require Export List.
Require Export Arith.
Require Export Lia.
Require Export Zwf.
Require Export Relations.
Require Export Inverse_Image.
Require Export Transitive_Closure.
Require Export Zdiv.

Open Scope nat_scope.

(** Equality up to associativity (and commutativity) *)


Theorem reflection_test :
 forall x y z t u v w:nat, (x+(y+z+(t+(u+v))))+w = (x+y+(z+(t+u)))+(v+w).
Proof.
Time intros; repeat rewrite Nat.add_assoc; auto.
Qed.

Theorem reflection_test2 :
 forall x y z t u v w:nat, 
(x+(y+z+(t+(u+v))))+w+(x+(y+z+(t+(u+v))))+w = (x+y+(z+(t+u)))+(v+w+((x+(y+z+(t+(u+v))))+w)).
Proof.
Time intros; repeat rewrite Nat.add_assoc; auto.
Qed.


Inductive bin : Set := node : bin->bin->bin | leaf : nat->bin.

Fixpoint flatten_aux (t fin:bin){struct t} : bin :=
  match t with
  | node t1 t2 => flatten_aux t1 (flatten_aux t2 fin)
  | x => node x fin
  end.

Fixpoint flatten (t:bin) : bin :=
  match t with
  | node t1 t2 => flatten_aux t1 (flatten t2)
  | x => x
  end.

Compute 
  flatten
     (node (leaf 1) (node (node (leaf 2)(leaf 3)) (leaf 4))).

Fixpoint bin_nat (t:bin) : nat :=
  match t with
  | node t1 t2 => bin_nat t1 + bin_nat t2
  | leaf n => n
  end.

Eval lazy beta iota delta [bin_nat] in
 (bin_nat
   (node (leaf 1) (node (node (leaf 2) (leaf 3)) (leaf 4)))).

Theorem flatten_aux_valid :
 forall t t':bin, bin_nat t + bin_nat t' = bin_nat (flatten_aux t t').
Proof.
 intros t; elim t; simpl; auto.
 intros t1 IHt1 t2 IHt2 t'; rewrite <- IHt1; rewrite <- IHt2.
 rewrite Nat.add_assoc; trivial.
Qed.

Theorem flatten_valid : forall t:bin, bin_nat t = bin_nat (flatten t).
Proof.
 intros t; elim t; simpl; auto.
 intros t1 IHt1 t2 IHt2; rewrite <- flatten_aux_valid; rewrite <- IHt2.
 trivial.
Qed.


Theorem flatten_valid_2 :
  forall t t':bin, bin_nat (flatten t) = bin_nat (flatten t')->
  bin_nat t = bin_nat t'.
Proof.
 intros; rewrite (flatten_valid t); rewrite (flatten_valid t');
 auto.
Qed.

Theorem reflection_test' :
 forall x y z t u:nat, x+(y+z+(t+u))=x+y+(z+(t+u)).
Proof.
 intros.
 change
   (bin_nat
      (node (leaf x)
         (node (node (leaf y) (leaf z))
               (node (leaf t)(leaf u)))) =
    bin_nat
      (node (node (leaf x)(leaf y))
         (node (leaf z)
               (node (leaf t)(leaf u))))).
 apply flatten_valid_2; auto.
Qed.

Ltac model v :=
  match v with
  | (?X1 + ?X2) =>
    let r1 := model X1 
              with r2 := model X2 in constr:(node r1 r2)
  | ?X1 => constr:(leaf X1)
  end.

Ltac assoc_eq_nat :=
  match goal with
  | [ |- (?X1 = ?X2 :>nat) ] =>
   let term1 := model X1 with term2 := model X2 in
   (change (bin_nat term1 = bin_nat term2);
    apply flatten_valid_2;
    lazy beta iota zeta delta [flatten flatten_aux bin_nat]; 
    auto)
  end.


Theorem reflection_test'' :
 forall x y z t u:nat, x+(y+z+(t+u)) = x+y+(z+(t+u)).
Proof.
 intros; assoc_eq_nat.
Qed.

(** In what follows, we make the previous tactic for 
associativity parametric over the type and binary operation, 
provided we have an associativity proof. 

In doing som we slightly change the representation to ease the
adaptation to commutativity by assuming that the leaves of the 
tree are not the values on our type, but they are natural numbers
refering to positions where one can find the values of type [A] 
in a valuation list. 

Look in detail to this implementation. 
*)

Section assoc_eq.
Variables (A : Type)(f : A->A->A)
  (assoc : forall x y z:A, f x (f y z) = f (f x y) z).

Fixpoint bin_A (l:list A)(def:A)(t:bin){struct t} : A :=
  match t with
  | node t1 t2 => f (bin_A l def t1)(bin_A l def t2)
  | leaf n => nth n l def
  end.

Theorem flatten_aux_valid_A :
 forall (l:list A)(def:A)(t t':bin),
 f (bin_A l def t)(bin_A l def t') = bin_A l def (flatten_aux t t').
Proof.
 intros l def t; elim t; simpl; auto.
 intros t1 IHt1 t2 IHt2 t';  rewrite <- IHt1; rewrite <- IHt2.
 symmetry; apply assoc.
Qed.

Theorem flatten_valid_A :
 forall (l:list A)(def:A)(t:bin),
   bin_A l def t = bin_A l def (flatten t).
Proof.
 intros l def t; elim t; simpl; trivial.
 intros t1 IHt1 t2 IHt2; rewrite <- flatten_aux_valid_A; now rewrite <- IHt2.
Qed.

Theorem flatten_valid_A_2 :
 forall (t t':bin)(l:list A)(def:A),
   bin_A l def (flatten t) = bin_A l def (flatten t')->
   bin_A l def t = bin_A l def t'. 
Proof.
 intros t t' l def Heq.
 rewrite (flatten_valid_A l def t); now rewrite (flatten_valid_A l def t').
Qed.

End assoc_eq.

Ltac term_list f l v :=
  match v with
  | (f ?X1 ?X2) =>
    let l1 := term_list f l X2 in term_list f l1 X1
  | ?X1 => constr:(cons X1 l)
  end.

Ltac compute_rank l n v :=
  match l with
  | (cons ?X1 ?X2) =>
    let tl := constr:(X2) in
    match constr:(X1 = v) with
    | (?X1 = ?X1) => n
    | _ => compute_rank tl (S n) v
    end
  end.

Ltac model_aux l f v :=
  match v with
  | (f ?X1 ?X2) =>
    let r1 := model_aux l f X1 with r2 := model_aux l f X2 in
      constr:(node r1 r2)
  | ?X1 => let n := compute_rank l 0 X1 in constr:(leaf n)
  | _ => constr:(leaf 0)
  end.

Ltac model_A A f def v :=
  let l := term_list f (nil (A:=A)) v in
  let t := model_aux l f v in
  constr:(bin_A A f l def t).

Ltac assoc_eq A f assoc_thm :=
  match goal with
  | [ |- (@eq A ?X1 ?X2) ] =>
  let term1 := model_A A f X1 X1 
  with term2 := model_A A f X1 X2 in
  (change (term1 = term2);
   apply flatten_valid_A_2 with (1 := assoc_thm); auto)
  end.

Theorem reflection_test3 :
 forall x y z t u:Z, (x*(y*z*(t*u)) = x*y*(z*(t*u)))%Z.
Proof.
 intros; assoc_eq Z Zmult Zmult_assoc.
Qed.



(** To work with commutativity we want a way to sort the 
value so let us implement the corresponding sorting functions *)

(** 1. Define a function [nat_le_bool (n m:nat) : bool] checking 
that n is less or equal to m. *)


Fixpoint insert_bin (n:nat)(t:bin){struct t} : bin :=
  match t with
  | leaf m => match nat_le_bool n m with
              | true => node (leaf n)(leaf m)
              | false => node (leaf m)(leaf n)
              end
  | node (leaf m) t' => match nat_le_bool n m with
                        | true => node (leaf n) t
                        | false => 
                            node (leaf m)(insert_bin n t')
                        end
  | t => node (leaf n) t
  end.

(** 2. define a [sort_bin (t:bin) : bin] function *)


Section commut_eq.

(** You will now extend the previous result to reason on commutativity as well

*)

 Variables (A : Type)(f : A->A->A).
 Hypothesis comm : forall x y:A, f x y = f y x.
 Hypothesis assoc : forall x y z:A, f x (f y z) = f (f x y) z.

(** Define the sorting interpretation function for bin trees 
looking for the values in the list, 
with a default value def which should not be used if the list is too short:

 Fixpoint bin_A' (l:list A)(def:A)(t:bin){struct t} : A.
*)


(** Prove the validity of flattening theorem: 
*)

 Theorem flatten_aux_valid_A' :
  forall (l:list A)(def:A)(t t':bin),
   f (bin_A' l def t)(bin_A' l def t') = bin_A' l def (flatten_aux t t').
 Proof.
Admitted.

 Theorem flatten_valid_A' :
  forall (l:list A)(def:A)(t:bin),
    bin_A' l def t = bin_A' l def (flatten t).
 Proof.
Admitted.

Theorem flatten_valid_A_2' :
 forall (t t':bin)(l:list A)(def:A),
   bin_A' l def (flatten t) = bin_A' l def (flatten t')->
   bin_A' l def t = bin_A' l def t'. 
Proof.
Admitted.

(** Inserting is correct wrt transformation f:*)

Theorem insert_is_f : forall (l:list A)(def:A)(n:nat)(t:bin),
   bin_A' l def (insert_bin n t) = 
   f (nth n l def) (bin_A' l def t).
Proof.
 intros l def n t; elim t.
 intros t1; case t1.
 intros t1' t1'' IHt1 t2 IHt2.
 simpl.
 auto.
 intros n0 IHt1 t2 IHt2.
 simpl.
 case (nat_le_bool n n0).
 simpl.
 auto.
 simpl.
 rewrite IHt2.
 repeat rewrite assoc; rewrite (comm (nth n l def)); auto.
 simpl.
 intros n0.
 case (nat_le_bool n n0); auto; try rewrite comm; auto.
Qed.


(** Prove now that sorting a bin tree does not change its interpretation :*)

Theorem sort_eq : forall (l:list A)(def:A)(t:bin),
    bin_A' l def (sort_bin t) = bin_A' l def t.  
Proof.
Admitted.


Theorem sort_eq_2 :
 forall (l:list A)(def:A)(t1 t2:bin),
   bin_A' l def (sort_bin t1) = bin_A' l def (sort_bin t2)->
   bin_A' l def t1 = bin_A' l def t2.  
Proof.
Admitted.

End commut_eq.


Ltac term_list' f l v :=
  match v with
  | (f ?X1 ?X2) =>
    let l1 := term_list' f l X2 in term_list' f l1 X1
  | ?X1 => constr:(cons X1 l)
  end.

Ltac compute_rank' l n v :=
  match l with
  | (cons ?X1 ?X2) =>
    let tl := constr:(X2) in
    match constr:(X1 = v) with
    | (?X1 = ?X1) => n
    | _ => compute_rank' tl (S n) v
    end
  end.

Ltac model_aux' l f v :=
  match v with
  | (f ?X1 ?X2) =>
    let r1 := model_aux' l f X1 with r2 := model_aux' l f X2 in
      constr:(node r1 r2)
  | ?X1 => let n := compute_rank' l 0 X1 in constr:(leaf n)
  | _ => constr:(leaf 0)
  end.

Ltac comm_eq' A f assoc_thm comm_thm :=
  match goal with
  | [ |- (?X1 = ?X2 :>A) ] =>
    let l := term_list' f (nil (A:=A)) X1 in
    let term1 := model_aux' l f X1 
    with term2 := model_aux' l f X2 in
    (change (bin_A' A f l X1 term1 = bin_A' A f l X1 term2);
      apply flatten_valid_A_2' with (1 := assoc_thm);
      apply sort_eq_2 with (1 := comm_thm)(2 := assoc_thm); 
      auto)
  end.

Theorem reflection_test4 : forall x y z:Z, (x+(y+z) = (z+x)+y)%Z.
Proof.
 intros x y z. comm_eq' Z Zplus Zplus_assoc Zplus_comm.
Qed.
