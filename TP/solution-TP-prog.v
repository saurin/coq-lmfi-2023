(** * Solution-TP-prog : Solution to Exercises on Programming in Coq **)

(** * Practice

** Functions and only that

**** Exercise 1 : function composition

Define a function [compose : forall A B C, (B->C)->(A->B)->(A->C)]. 
Test it with functions [S] and [pred] on natural numbers (type [nat]).
*)

Section composition.
Variables A B C: Type.

Definition compose1 (f:B->C)(g:A->B)(x: A) := f (g x).

Definition compose2 := fun (f:B->C)(g:A->B)(x: A) => f (g x).

Definition compose3 (f:B->C)(g:A->B) := fun x => f (g x).

Definition compose4 := fun (f:B->C) g (x: A) => f (g x).

Definition compose5 := fun f (g:A -> B) x => f (g x):C.

Check compose1.
Check compose2.
Check compose3.
Check compose4.
Check compose5.

End composition.

Check compose1.
Check compose2.
Check compose3.
Check compose4.
Check compose5.

Definition compose6 (A B C: Type) (f:B->C)(g:A->B) (x: A):= f (g x).

Check compose6.

Compute compose6 nat nat nat S pred 7.
Compute compose6 nat nat nat S pred 0.
Compute compose6 nat nat nat pred S 0.

Definition composenat := compose6 nat nat nat.

Check composenat.

Compute composenat S pred 7.
Compute composenat S pred 0.
Compute composenat pred S 0.

(** if one describes precisely the type of [compose]
it is not needed to provide the type of each arguments 
and even A B C can be left implicit, using a generic 
[_] pattern: *)

Definition compose7 : forall A B C, (B->C)->(A->B)->A->C :=
fun _ _ _ f g x => f (g x).

Check compose7.


Definition compose8 {A B C} (f:B->C)(g:A->B) := fun x => f (g x).


(** Here, the arguments A B C of [compose] are treated 
implicitly, they are implicit arguments of the function
which are not required to be passed to the function as 
being inferred from the other arguments (here, [f], [g] 
and [x].. 
*)

Check compose8.

Compute compose8 S pred 7.
Compute compose8 S pred 0.
Compute compose8 pred S 0.

(** Still, it is possible to deactivate the implicit 
argument to specialise a function as for the composition 
on functions over nat: *)

Definition composenat' := @compose7 nat nat nat.

Check composenat'.

Definition compose {A B C} := @compose8 A B C.

(** **** Exercise 2 : Boolean ersatz

Define (without using [bool] nor any other inductive type):

- a type [mybool : Type];
- two constants [mytrue] and [myfalse] of type [mybool];
- a function [myif : forall A, mybool -> A -> A -> A] such that 
 [myif mytrue x y] computes to [x] and [myif myfalse x y] computes to [y].

*)

(* *)

(** Similarly to the type [forall X, X -> X], inhabited by 
only one value, the identity function and which can 
therefore serve as a [unit] type, one can define a type with two 
elements which is known as the church encoding of booleans: *)

Definition mybool := forall X, X->X->X.

(** That type is indeed inhabited by two functions only, 
the first and second projections: *)

Definition mytrue : mybool := fun _ x y => x.
Definition myfalse : mybool := fun _ x y => y.

(** On can therefore define a conditional branching based 
on the operation behaviour of [mytrue] and [myfalse]: 
we want [myif b x y] to return [x] if 
[b] is [mytrue] (ie the first projection) and
to return [y] if 
[b] is [myfalse] (ie the second projection). 
In both cases, [myif b x y] shall behave like [b x y]... 
from which we infer the definition: *)


Definition myif : forall {Y}, mybool -> Y -> Y -> Y :=
 fun _ b x y => b _ x y.

Compute myif mytrue 0 1.
Compute myif myfalse 0 1.



(** **** Exercise 3 : Church numerals

Encode in Coq the Church numerals, for which the number 
<<n>> is represented by <<λf.λx.(f (f (... (f x))))>> where 
<<f>> is applied <<n>> times.

More precisely, define (without using [nat] nor any other inductive type):

 - a type [church : Type]
 - two constant [zero] and [one] of type [church]
 - a function [church_succ] of type [church->church]
 - two functions [church_plus] and [church_mult] of type [church->church->church]
 - a function [church_power]
 - a test [church_iszero]

Also define two functions [nat2church : nat -> church] and [church2nat : church -> nat] 

*)

(* *)

(** Let us first analyze the type of Church numerals:
the statement of the exercise tells us that church is 
a function of two arguments, [f] and [x], which iterates 
[f] a certain number of time over [x]. 

Therefore [f] shall have a type of the form [X -> Y] 
and necessarily [X] must be the type of [x]. Moreover, 
since [f] can be iterated, the type of its codomain shall 
be the same as the type of its domain: [X]=[Y]. 
Moreover, we want this type to apply on any pair of arguments 
the first of which is iterable on the second, and we thus 
introduce a universal quantification (dependent product type)
ending up with [forall X, (X->X)->(X->X)] as a type of 
Church numerals.

*)

Set Implicit Arguments. 
Set Universe Polymorphism.


Definition church := forall X, (X->X)->(X->X).

(** in defining [zero, one, ...], it is not 
needed to give the type of each argument (and neither must 
we provide a name for the type argument [X]) 
when the type of the constant is given : *)

Definition zero : church := fun _ f x => x.
Definition one : church := fun _ f x => f x.

(** in fact there is another candidates for one: *)

Definition onebis : church := fun _ f => f.
Definition two : church := fun _ f x => f (f x).

Definition succ : church -> church :=
 fun n => fun _ f x => n _ f (f x).

Compute succ one.

Definition church2nat (n:church) := n _ S 0.

Compute church2nat zero.
Compute church2nat one.
Compute church2nat onebis.
Compute church2nat two.

Fixpoint nat2church (n:nat) :=
 match n with
 | O => zero
 | S m => succ (nat2church m)
 end.


Compute nat2church 3.
Compute church2nat (nat2church 100).

(** In the following, we will use the ability of Coq 
to not require that we actually provide the actual 
type when it can be inferred to lighten the definitions.

But to be fully informative and in order to understand 
what happens with church_power, we provide below the 
definition of those functions providing explicitly the 
type instanciations: *)

Definition church_plus (n m:church) : church :=
  fun _ f x => n _ f (m _ f x).


Compute church_plus one two.
Compute church2nat (church_plus (nat2church 13) (nat2church 10)).

Definition church_mult (n m:church) : church :=
 fun _ f =>  n _ (m _ f).

Compute church_mult one two.
Compute church2nat (church_mult (nat2church 13) (nat2church 10)).

Definition church_pow (n m:church) : church :=
 fun _ =>  m _ (n _).


Compute church_pow two two.
Compute church2nat (church_pow (nat2church 2) (nat2church 5)).

(** To compute whether n is equal to zero of not, we 
pass two arguments to n. The first one will be iterated 
n times on the second one. 
- Therefore, if n is zero, the first function is not 
iterated and the second argument is returned directly, 
this second argument should be [true]. 
- Otherwise, if n is not zero, the function will be 
iterated at least one and we would like is_zero n to 
return false, therefore it is sufficient that the 
first argument of n is the the constant function 
always returning zero, independently of its argument: 
[fun _ => false]. *)


Definition is_zero (n : church) : bool :=
  n _ (fun _ => false) true.

Compute is_zero zero.
Compute is_zero one.

(** Explicit typing of the above functions: *)

Definition church_plus_explicit (n m:church) : church :=
  fun X f x => n X f (m X f x).


Definition church_mult_explicit (n m:church) : church :=
 fun X f =>  n X (m X f).

Definition church_pow_explicit (n m:church) : church :=
 fun X =>  m (X->X) (n X).

Definition is_zero_explicit (n : church) : bool :=
  n bool (fun (b:bool) => false) true.




(** how to compute the predecessor?

we want that, to [zero], it associates [zero]
and, to [fun f x => f (f ... x)], it associates
[fun f x => f ... x] *)

Definition church_pred (n:church) : church :=
 fun _ f x =>
   n _ (fun g h => h (g f)) (fun _ => x) (fun u => u).

Definition church_pred_explicit (n:church) : church :=
 fun X f x =>
   n ((X->X)->X) (fun g h => h (g f)) (fun _ => x) (fun u => u).

Compute church2nat (church_pred zero).
Compute church2nat (church_pred one).
Compute church2nat (church_pred (nat2church 13)).



(** Attention, donne [universe inconsistency] si les univers
    polymorphes ne sont pas activés. *)

Definition church_minus (n m : church) : church :=
  m _ church_pred n.

Compute church2nat (church_minus two two).
Compute church2nat (church_minus (nat2church 10) (nat2church 7)).




(** ** Base types

**** Exercise 4 : Booleans

- Write a function [checktauto : (bool->bool)->bool] which tests whether 
 a Boolean unary function always answers [true] (difficulty: easy).
- Same for [checktauto2] and [checktauto3] for Boolean functions expecting 2, 
 then 3 arguments. This can be done by enumerating all cases, but there is a 
 clever way to proceed (for instance by re-using [checktauto] 
 (difficulty: medium).
- Check whether [fun a b c => a || b || c || negb (a && b) || negb (a && c)] is 
 a tautology (difficulty: easy).
 Note : the command [Open Scope bool_scope.] activates notations [||] and [&&] 
 (respectively for functions [orb] and [andb]).
- Define some functions behaving like Coq standard functions [negb] and [orb] 
 and [andb]. (difficulty: easy)
*)

(* *)

Open Scope bool_scope.

Definition checktauto f := f true && f false.
Definition checktauto2 f := checktauto (compose checktauto f).
Definition checktauto3 f := checktauto (compose checktauto2 f).

Compute checktauto3
        (fun a b c => a || b || c || negb (a && b) || negb (a && c)).
Compute checktauto3 (fun a b c => a || b || c).

(** A revoir lors du cours sur les "types dépendants" :
    une généralisation de checktauto aux fonctions booléennes
    à n arguments. P.ex. nbool 2 = bool -> bool -> bool
    Et nchecktauto 2 se comportera comme le checktauto2 précédent. *)

Fixpoint nbool n : Type :=
 match n with
 | 0 => bool
 | S n => bool -> nbool n
 end.

Fixpoint nchecktauto n : nbool n -> bool :=
 match n with
 | 0 => fun b => b
 | S n => fun f => nchecktauto n (f true) && nchecktauto n (f false)
 end.

Compute nchecktauto 3
        (fun a b c => a || b || c || negb (a && b) || negb (a && c)).
Compute nchecktauto 3 (fun a b c => a || b || c).



(** **** Exercise 5 : usual functions on natural numbers.

Define the following functions of type [nat  -> nat -> nat] 
(without using the ones of Coq standard library of course!):

- [addition] (difficulty: easy)
- [multiplication] (difficulty: easy)
- [subtraction] (difficulty: medium)
- [power] (difficulty: medium)
- [modulo] such that [modulo a b] computes a modulo b 
 (difficulty: medium)
- [gcd] (difficulty: hard)

Ackermann-Péter (difficulty: hard):
We recall that the Ackermann-Péter function, AP, is defined as: 
- AP(0,n) = n+1
- AP(m+1, 0) = AP(m, 1)
- AP(m+1, n+1) = AP(m, AP(m+1, n)).

Try defining [AP], of type [nat -> nat -> nat], in the 
most natural way, based on its definition. 
What problem do you encounter?

What possible workaround can you imagine?

*)

Require Import Arith.

Fixpoint add n m :=
 match n with
 | 0 => m
 | S n => S (add n m)
 end.

Compute add 3 7.

Fixpoint mul n m :=
 match n with
 | 0 => 0
 | S n => add m (mul n m)
 end.

Compute mul 3 7.

Fixpoint sub n m :=
 match n, m with
 | _,0 => n
 | 0,_ => n
 | S n, S m => sub n m
 end.

Compute sub 7 3.
Compute sub 3 7.

Fixpoint fact n :=
 match n with
 | 0 => 1
 | S m => mul n (fact m)
 end.

Compute fact 7.
Fail Compute fact 8.


Fixpoint pow a b :=
 match b with
 | 0 => 1
 | S b => mul a (pow a b)
 end.

Compute pow 2 10.

(* En Coq 8.4, ajouter:
Require Import NPeano.
Infix "=?" := Nat.eqb (at level 70, no associativity) : nat_scope.
*)

Fixpoint modulo_loop a b n :=
 match n with
 | 0 => 0
 | S n =>
   if a <? b then a
   else modulo_loop (sub a b) b n
 end.

Definition modulo a b := modulo_loop a b a.

Compute modulo 10 3.
Compute modulo 3 0.

Fixpoint gcd_loop a b n :=
  match n with
  | 0 => 0
  | S n =>
    if b =? 0 then a
    else gcd_loop b (modulo a b) n
  end.

Definition gcd a b := gcd_loop a b (S b).

Compute gcd 17 23.
Compute gcd 23 17.
Compute gcd 12 9.
Compute gcd 9 12.
Compute gcd 10 1.
Compute gcd 1 10.
Compute gcd 0 7.
Compute gcd 7 0.


Fail Fixpoint AP (m n: nat) :=
match (m,n) with 
| (0 , _) => n+1
| (S m', 0) => AP m 1
| (S m', S n') => AP m' (AP m n)
end.

Fixpoint AP (m: nat) : nat -> nat :=
match m with 
| 0 => (fun x => x+1)
| S m' => 
fix APaux (n: nat) := match n with 
| O => AP m' 1
| S n' => AP m' (APaux n')
end
end.

(* *)

(** **** Exercise 6 : Fibonacci

- Define a function [fib] such that [fib 0 = 0], [fib 1 = 1] 
 then [fib (n+2) = fib (n+1) + fib n].  (you may use a [as] 
 keyword to name some subpart of the [match] pattern ("motif" en français)).
- Define an optimized version of [fib] that computes faster that 
 the previous one by using Coq pairs.
- Same question with just natural numbers, no pairs. 
 Hint: use a special recursive style called "tail recursion".
- Load the library of binary numbers via [Require Import NArith].
 Adapt you previous functions for them now to have type [nat -> N].
 What impact does it have on efficiency ?
 Is it possible to simply obtain functions of type [N -> N] ?
*)

(* *)


Fixpoint fib n :=
 match n with
 | 0 => 0
 | 1 => 1
 | S ((S n) as p) => fib p + fib n
 end.

Compute List.map fib (List.seq 0 20).


(** Un premier exemple de test de fonction
    via une specification executable *)

Definition fib_spec n := fib (n+2) =? fib n + fib (n+1).

Time Compute List.forallb fib_spec (List.seq 0 20).

Fixpoint fib2 n :=
  match n with
  | 0 => (0,1)
  | S n => let (a,b) := fib2 n in (b,a+b)
  end.

Definition fib_opt n := fst (fib2 n).

Compute List.map fib_opt (List.seq 0 20).

Fixpoint fibloop n a b :=
  match n with
  | 0 => a
  | S n => fibloop n b (a+b)
  end.

Definition fib_tailrec n := fibloop n 0 1.

Compute List.map fib_tailrec (List.seq 0 20).

(** By trying to compute "large" fibonacci numbers, one gets
    "stack overflow" with the three previous versions.
    In fact, it is a problem with the display of [nat] which cannot go that 
    far! A trick it to traslate to [N] (binary numbers) before displaying the 
    result. *)

Fail Time Compute ((fib 35)).

Require Import NArith.

Time Compute (N.of_nat (fib 35)). (** about 4s *)
Time Compute (N.of_nat (fib_opt 35)). (** about 1.2s *)
Time Compute (N.of_nat (fib_tailrec 35)).  (** about 1s *)

(** Better: one can actually do the additions in binary arithmetic! *)

Fixpoint fibN n :=
 match n with
 | 0 => 0%N
 | 1 => 1%N
 | S ((S n) as p) => (fibN p + fibN n)%N
 end.

Check fibN.

Fixpoint fibN2 n :=
  match n with
  | 0 => (0,1)%N
  | S n => let (a,b) := fibN2 n in (b,a+b)%N
  end.

Check fibN2.

Definition fibN_opt n := fst (fibN2 n).

Fixpoint fibNloop n a b :=
  match n with
  | 0 => a
  | S n => fibNloop n b (a+b)%N
  end.

Definition fibN_tailrec n := fibNloop n 0%N 1%N.

Check fib_tailrec.


Time Compute (fibN 35). (** 2s *)
Time Compute (fibN_opt 35). (** 0s *)
Time Compute (fibN_tailrec 35).  (** 0s *)

Time Compute fibN_tailrec (10*1000). (** 10s *)
Compute N.log2 (fibN_tailrec (10*1000)).
(** fib 10000 requires arount 7000 bits, that it 
    approximately 2000 decimals numbers *)

(** NB: using 10*1000 is done to avoid a crash of the parser of [nat],
    which gets "stack overflow" for constants above 5000. *)

(** Can we completely get rid of [nat] ?
    That is not so simple to remain structurally decreasing in that case.
    One can try using [N.peano_rect]. *)

Check N.peano_rect.

(** A mystery consists in how to do such a [N.peano_rect] ?
    Cf [Print Pos.peano_rect] and see that it is indeed 
    structurally decreasing. *)

Print Pos.peano_rect.

Definition fibNN2 n :=
  N.peano_rect
    _
    (0,1)%N
    (fun n p => let '(a,b) := p in (b,a+b)%N)
    n.

Definition fibNN n := fst (fibNN2 n).

(** Note: Argument [_] hidden in [fibNN2] is [(fun _ => (N*N)%type)].
    This shows that one uses [N.peano_rect] in a  *non-dependent* way
    (results are always of the same type, independently of [n]).
*)


Require Import List.
Import ListNotations.

Compute List.map fibNN [0;1;2;3;4;5;6;7;8;9;10]%N.

Time Compute fibNN 10000.

Definition fibNNloop n :=
 N.peano_rect
   _
   (fun a b => a)
   (fun _ p a b => p b (a+b)%N)
   n.

Definition fibNN_tail n := fibNNloop n 0%N 1%N.

Compute List.map fibNN [0;1;2;3;4;5;6;7;8;9;10]%N.

Time Compute fibNN_tail 10000.



(** **** Exercise 7 : Fibonacci though matrices

- Define a type of 2x2 matrices of numbers (for instance via a quadruple).
- Define the multiplication and the power of these matrices.
 Hint: the power may use an argument of type [positive].
- Define a fibonacci function through power of the following matrix:

<<
1 1
1 0
>>
*)

(* *)

Definition mat : Type := N * N * N * N.


Definition fibmat : mat := (1,1,1,0)%N.

Definition multmat (u v : mat) : mat :=
  let '(u11,u12,u21,u22) := u in
  let '(v11,v12,v21,v22) := v in
  (u11*v11+u12*v21, u11*v12+u12*v22,
   u21*v11+u22*v21, u21*v12+u22*v22)%N.

Compute multmat fibmat fibmat.

Fixpoint powmat m p :=
  match p with
  | 1 => m
  | p~0 => let m' := powmat m p in multmat m' m'
  | p~1 => let m' := powmat m p in multmat m (multmat m' m')
  end%positive.

Definition fib_m p :=
  let '(_,a,_,_) := powmat fibmat p in a.

Compute List.map fib_m [1;2;3;4;5;6;7;8;9;10]%positive.

Time Compute fibNN (10000). (* 1s *)
Time Compute fib_m (10000). (* 2s *)


(** **** Exercise 8 : Fibonacci decomposition of numbers

We aim here at programming the Zeckendorf theorem in practice : 
every number can be decomposed in a sum of Fibonacci numbers, and moreover 
this decomposition is unique as soon as these Fibonacci numbers are 
distinct and non-successive and with index at least 2.

Load the list library: *)

Require Import List.
Import ListNotations.

(**
- Write a function [fib_inv : nat -> nat] such that if 
 [fib_inv n = k] then [fib k <= n < fib (k+1)].
- Write a function [fib_sum : list nat -> nat] such that 
 [fib_sum [k_1;...;k_p] = fib k_1 + ... + fib k_p].
- Write a function [decomp : nat -> list nat] such that 
 [fib_sum (decomp n) = n] and [decomp n] does not contain 0 
 nor 1 nor any redundancy nor any successive numbers.
- (Optional) Write a function [normalise : list nat -> list nat] 
 which receives a decomposition without 0 nor 1 nor redundancy, 
 but may contains successive numbers, and builds a decomposition 
 without 0 nor 1 nor redundancy nor successive numbers. You might 
 assume here that the input list of this function is sorted in the 
 way you prefer. 

**)


Fixpoint fib_inv_loop n a b k cpt :=
  match cpt with
  | 0 => 0
  | S cpt =>
    if n <? b then k
    else fib_inv_loop n b (a+b) (S k) cpt
  end.

Definition fib_inv n :=
 if n =? 0 then 0 else fib_inv_loop n 1 2 2 n.

Compute fib_inv 0.
Compute fib_inv 1.
Compute fib_inv 2.
Compute fib_inv 5.
Compute (fib 5, fib 6).
Compute fib_inv 10.
Compute (fib 6, fib 7).
Compute fib_inv 1000.
Compute (fib 16, fib 17).

(** more systematic test, via an execu specification. *)

Definition fib_inv_spec n :=
 (n =? 0) ||
 (let k := fib_inv n in
   (fib k <=? n) && (n <? fib (S k))).

Compute List.forallb fib_inv_spec (List.seq 0 1000).

Fixpoint decomp_loop n cpt :=
 match cpt with
 | 0 => []
 | S cpt =>
   if n =? 0 then []
   else
     let k := fib_inv n in
     k :: (decomp_loop (n-fib k) cpt)
 end.

Definition decomp n := decomp_loop n n.

Fixpoint sumfib l :=
  match l with
  | [] => 0
  | k::l => fib k + sumfib l
  end.

Compute decomp 10.
Compute sumfib [6;3].

Compute decomp 100.
Compute sumfib [11;6;4].

(** Executable specification of [decomp].
    This is a partial specification : one does not check that
    the resulting list has no repetition no consecutive integers. *)

Definition decomp_spec n :=
 sumfib (decomp n) =? n.

Compute List.forallb decomp_spec (seq 0 1000).

(** Bonus : normalisation of a repetition-free decomposition already 
sorted in increasing order. *)

Fixpoint normalise_loop l n :=
 match n with
 | 0 => l
 | S n =>
   match l with
   | [] => []
   | x::l =>
     match normalise_loop l n with
     | [] => [x]
     | y::l' =>
       if y =? x+1
       then normalise_loop ((y+1)::l') n
       else x::y::l'
     end
   end
 end.

Definition normalise l := normalise_loop l (length l).

Compute normalise [2;3;4;5;6].
Compute (sumfib [2;3;4;5;6], sumfib [2;5;7]).

Compute normalise [2;3;4;5;6;7].
Compute (sumfib [2;3;4;5;6;7], sumfib [4;6;8]).

(** Bonus : equation of the function that decreases by 1 
the decompositions. *)

Definition g n := sumfib (List.map pred (decomp n)).

Definition g_spec n := (g n =? n - g (g (n-1))).

Compute List.forallb g_spec (seq 0 1000).

(* *)

(** **** Exercise 9: Binary trees with distinct internal and external nodes.

By taking inspiration from the definition of lists above, define an 
inductive type [iotree] depending on two types [I] and [O]
such that every internal node is labelled with an element of type I 
and every leaf is labelled with an element of type O. 

*)

Inductive iotree (I O: Type):=
| oleaf: O -> iotree I O
| inode : I -> iotree I O -> iotree I O -> iotree I O.


(* *)

(** **** Exercise 10: Lists alternating between two types.

By taking inspiration from the definition of lists above, define an 
inductive type [ablists] depending on two types [A] and [B] which is 
constituted of lists of elements of types alternating between [A] and [B]
*)

(* 

[[
Inductive ablist A B: Type:=
| abnil: ablist A B
| abcons: A -> ablist B A -> ablist A B.

Fixpoint alternate {A B : Type} (l: list (A * B)) : ablist A B :=
match l with 
| nil => abnil A B
| (a,b)::l' => abcons A B a (abcons B A b (alternate l'))
end.

Fixpoint abpairing {A B: Type} (l: ablist A B) : list (A * (option B)) :=
match l with 
| abnil _ _ => nil (* A * option B *)
| abcons _ _ a (abnil _ _ ) => cons (*A * option B*) (a, (None)) nil
| abcons _ _  a (abcons _ _  b l') => cons (a,Some b) (abpairing l')
end.
]]

*)


Section ablist.

Variables  A B: Type. 

Inductive ablist :=
| abnil: ablist
| abcons: A -> balist -> ablist
with
balist :=
|banil : balist
|bacons: B -> ablist -> balist. 

Fixpoint alternate (l: list (A * B)) : ablist :=
match l with 
| nil => abnil
| (a,b)::l' => abcons a (bacons b (alternate l'))
end.

Fixpoint abpairing (l: ablist) : list (A * (option B)) :=
match l with 
| abnil => nil
| abcons a (banil) => cons (a, (None)) nil
| abcons a (bacons b l') => cons (a,Some b) (abpairing l')
end.

Check alternate.

Check abpairing.


End ablist.

Check alternate.

Check abpairing.

Compute alternate  [(true, 0); (false,1); (false,2)]. 

(* *)

(** ** Programming with lists *)

(** In the exercises below, you should have loaded the 
following Coq libraries: *)

Require Import Bool Arith List.
Import ListNotations.
Set Implicit Arguments.


(** **** Exercise 11: Classical exercises on lists

Program the following functions, without using the 
corresponding functions from the Coq standard library :

 - [length]
 - concatenate ([app] in Coq, infix notation [++])
 - [rev] (for reverse, a.k.a mirror)
 - [map : forall {A B}, (A->B)-> list A -> list B]
 - [filter : forall {A}, (A->bool) -> list A -> list A]
 - at least one [fold] function, either [fold_right : 
 forall A B : Type, (B -> A -> A) -> A -> list B -> A] or 
 [fold_left: forall A B : Type, (A -> B -> A) -> list B -> A -> A]. 
Their behaviour is defined as: 
[fold_right f a [b1; ...; bk] = f b1 (f b2 (... (f bk a)...))]
and 
[fold_left f a [b1; ...; bk] = f (... (f (f a b1) b2 )...) bk)]
The fold functions take an 
operation a function with inputs in 
A and B that returns an element in A, an starting element of 
type A and a list of element of type B and 
 - [seq : nat -> nat -> list nat], such that 
  [seq a n = [a; a+1; ... a+n-1]]

Why is it hard to program function like [head], [last] 
or [nth] ? How can we do ?
*)

(* The inductive definition of lists, once again: *)
Print list.

(* Syntax about lists, provided by module ListNotations imported above *)
Check []. (* is equivalent to : nil *)
About "::". (* is equivalent to : cons *)

Check 1::[].

Check [1;2;3].
(* is equivalent to *)
Check 1::(2::(3::[])).

(* A first function on lists : length *)

Fixpoint length {A} (l:list A) :=
 match l with
 | [] => 0
 | x::q => S (length q)
 end.

Check @length.
Compute length [1;2;3].
Compute length [true;false;false].
Check [[1;2];[3];[];[5;6]].

(*Set Printing All. (*If you want to see all the implicit arguments*)*)
Check length [[1;2];[3];[];[5;6]].
Compute length [[1;2];[3];[];[5;6]].

(* Concatenation *)

Fixpoint app {A} (l1 l2 : list A) :=
 match l1 with
 | [] => l2
 | x1::q1 => x1 :: app q1 l2
 end.

Check app [1;2;3] [4;5].

(* Using Coq standard definition of app: *)
Check [1;2;3]++[4;5].

(* Reverse (in the "slow" definition, i.e. quadratic complexity) *)

Fixpoint rev {A} (l : list A) :=
 match l with
 | [] => []
 | x::q => rev q ++ [x]
 end.

(*
rev [1;2;3;4]
 = rev [1;2;3] ++ [4]
 = (rev [1;2]) ++ [3]) ++ [4]
 = (rev [1]) ++ [2]) ++ [3]) ++ [4]
 = ([] ++ [1]) ++ [2]) ++ [3]) ++ [4]
      cost 0  cost 1   cost 2  cost 3
 total cost proportional to (n*(n+1)/2) when n is length l
 i.e. quadratic
*)

(* Efficient reverse : we define first an auxiliary function called
   rev_app, which is almost app, but with the left list being reversed.*)

Fixpoint rev_app {A} (l1 l2 : list A) :=
 match l1 with
 | [] => l2
 | x1::q1 => rev_app q1 (x1::l2)
 end.

Compute rev_app [1;2;3] [4;5].

(* And then : *)

Definition fast_rev {A} (l:list A) := rev_app l [].

Compute fast_rev [1;2;3]. (* linear in the size of l *)

(* map and filter : pretty straightforward *)

Fixpoint map {A B} (f:A->B) l :=
 match l with
 | [] => []
 | x::q => f x :: map f q
 end.

Fixpoint filter {A} (f:A->bool) l :=
 match l with
 | [] => []
 | x::q => if f x then x :: filter f q else filter f q
 end.


(* fold *)

Compute fold_right Nat.add 0 [1;2;3]. (* sums all the numbers *)
Compute fold_left Nat.add [1;2;3] 0.  (* the same... *)

Check fold_right.
(* forall A B : Type, (B -> A -> A) -> A -> list B -> A *)

(* fold_right f a [x1;x2;x3] =
   (f x1 (f x2 (f x3 a))).
*)

Fixpoint fold_right {A B} (f:B->A->A) a l :=
 match l with
 | [] => a
 | x::q => f x (fold_right f a q)
 end.

Check fold_left.
(* : forall A B : Type, (A -> B -> A) -> list B -> A -> A *)

(* fold_left f [x1;x2;x3] a =
   f (f (f a x1) x2) x3.
*)

Fixpoint fold_left {A B} (f:A->B->A) l a :=
 match l with
 | [] => a
 | x::q => fold_left f q (f a x)
 end.

(* Let's use folds to rebuild a list *)

Compute fold_right (fun x l => x::l) [] [1;2;3].
Compute fold_left (fun l x => x::l) [1;2;3] [].

(* seq *)

Fixpoint seq start len :=
  match len with
  | 0 => []
  | S len' => start :: seq (S start) len'
  end.

(* head with the option type *)

Definition head {A} (l:list A) : option A :=
 match l with
 | [] => None
 | x::q => Some x
 end.

Compute head [1;2;3].

(* head with a default answer in the case of empty list *)

Definition head_dft {A} (l:list A) (dft:A) : A :=
 match l with
 | [] => dft
 | x::q => x
 end.

Compute head_dft [1;2;3] 0.

(* other approaches :
 - conditions ensuring that the list isn't empty
   (proof-carrying code, quite complex)
 - switch from list to a vector (list of a given size)
   head : vector A (S n) -> A
   (dependent-type programming)
*)

Fixpoint last {A} (l:list A) : option A :=
 match l with
 | [] => None
 | [a] => Some a
 | _::l => last l
 end.

Compute last [1;2;3].

Fixpoint nth {A} n (l:list A) (dft:A) :=
 match n, l with
 | _, [] => dft
 | O, x::_ => x
 | S n, _::q => nth n q dft
 end.

(* Left as exercise : last with default value or nth with option *)






(* *)
(** **** Exercise 12:  Some executable predicates on lists

 - [forallb : forall {A}, (A->bool) -> list A -> bool].
 - [increasing] which tests whether a list of numbers is 
  strictly increasing.
 - [delta] which tests whether two successive numbers of 
  the list are always apart by at least [k].
 
*)


Fixpoint forallb {A} (f:A->bool) l :=
 match l with
 | [] => true
 | x::q => f x && forallb f q
        (* or more lazy:
           if f x then forallb f q else false *)
 end.

Fixpoint increasing l :=
 match l with
 | [] => true
 | [x] => true
 | x::((y::_) as q) => (x <? y) && increasing q
 end.
(** As shown in live during the video-session :
    be careful not to forget the (  ) around y::_ !!
*)

(* Or in two steps (longer but more basic hence more robust) *)

Fixpoint larger_than x l :=
 match l with
 | [] => true
 | y::q => (x <? y) && larger_than y q
 end.

Definition increasing_v2 l :=
 match l with
 | [] => true
 | x::q => larger_than x q
 end.

(* Or more generic : *)

Fixpoint forallb_successive {A} (f:A->A->bool) l :=
 match l with
 | [] => true
 | [x] => true
 | x::((y::_) as q) => f x y && forallb_successive f q
 end.

Definition increasing' := forallb_successive Nat.ltb.
(* where Nat.ltb is the function behind the <? test *)

Compute increasing [1;2;3;4].
Compute increasing [1;2;2].

Definition delta k := forallb_successive (fun a b => a+k <=? b).

Compute delta 1 [1;2;3;4].


(* *)
(** **** Exercise 13: Mergesort

- Write a [split] function which dispatch the elements of 
 a list into two lists of half the size (or almost). It is 
 not important whether an element ends in the first or second 
 list. In particular, concatenating the two final lists will 
 not necessary produce back the initial one, just a permutation 
 of it.
- Write a [merge] function which merges two sorted lists into a 
 new sorted list containing all elements of the initial ones. 
 This can be done with structural recursion thanks to a inner 
 [fix] (see [ack] in the session 3 of the course). 
 - Write a [mergesort] function based on the former functions.
*)

(* First, a solution by computing first the desired size *)

Fixpoint split_at {A} (l:list A) n :=
 match l, n with
 | [], _ => ([], [])
 | _, 0 => ([],l)
 | x::q, S n' =>
   let '(l1,l2) := split_at q n'
   in (x::l1,l2)
 end.

Definition split {A} (l:list A) := split_at l (length l / 2).

Compute split [1;2;3;4;5].

(* Second a solution that just alternates *)

Fixpoint split' {A} (l:list A) :=
 match l with
 | [] => ([], [])
 | x::q =>
   let (l1,l2) := split' q in
   (x::l2,l1)
 end.

Compute split' [1;2;3;4;5].

(* Merge : *)

Fixpoint merge (l:list nat) : list nat -> list nat :=
    match l with
    | [] => fun s => s
    | a::l' => fix merge_l s :=
        match s with
        | [] => l
        | b::s' =>
            if a <? b then a::(merge l' s)
            else b::merge_l s'
        end
    end.

Compute merge [1;3;4;5] [2;3;5;7].

(* Mergesort *)

Fixpoint mergesort_counter l n :=
  match n with
  | O => l
  | S n =>
    match l with
    | [] => [] (* be careful not to forget "little" cases, otherwise
                  you may "loop" (or rather exhaust the counter n) *)
    | [x] => [x]
    | _ =>
      let (l1,l2) := split' l in
      merge (mergesort_counter l1 n) (mergesort_counter l2 n)
    end
  end.

Definition mergesort l := mergesort_counter l (length l).

Compute mergesort [1;7;2;3;10;0;3].



(* *)
(** **** Exercise 14: Powerset

Write a [powerset] function which takes a list [l] and returns 
the list of all subsets of [l].
For instance [powerset [1;2] = [[];[1];[2];[1;2]]]. 
The order of subsets in the produced list is not relevant.

*)


Fixpoint powerset {A} (l:list A) : list (list A) :=
 match l with
 | [] => [[]]
 | x::q =>
   let ll := powerset q in
   ll ++ (map (fun l => x::l) ll)
        (* or: map (cons x) ll *)
 end.


Compute powerset [1;2;3].


(* *)

(* *)
(** **  Lists with Fast Random Access


We consider here data structures that are purely functional (also said 
_persistent_ or _immutable_) and allow us to encode _lists_. We have 
already seen the Coq standard implementation of lists, but more 
generally a list is here a finite sequence of elements  where the 
order of elements in the list is meaningful and where the operations 
"on the left" are efficient, both a [cons] extension operation and a 
[head] and a [tail] access functions.

Actually, instead of two separated [head] and [tail] 
functions, we will consider here a unique function 
[uncons] doing both. More precisely, [uncons l = Some 
(h,t)] whenever the list has a head [h] and a tail [t] 
and [uncons l = None] whenever [l] is empty.

We will also focus on a [nth] function for our lists, 
allowing us to perform "random" access anywhere in a 
list, not just on the left.

The goal of this work is to implement lists in various 
manners, ending with [nth] functions of logarithmic 
complexity without compromising too much the cost of 
functions [cons] and [uncons] (and ideally keeping this 
cost constant). Here, the complexity we consider is the 
number of access to any part of any innner substructure, 
in the worst case, expressed in function of the number of 
elements present in the list. In a first time, we neglect 
the cost of any arithmetical operations we may perform 
on integers.
*)

(* *)
(**  **** Exercise 15 : Implementation via regular Coq lists

Start a _module_ to isolate the code of this exercise from 
the next ones (Modules will be studied in future lectures): *)

Require Import Arith List.
Import ListNotations.

Set Implicit Arguments.

Module RegularList.

Print List.

(** 
Implement the following operations on the Coq usual [list] 
datatype, and give their complexity.

- [cons : forall {A}, A -> list A -> list A].
- [uncons : forall {A}, list A -> option (A * list A)].
- [nth : forall {A}, list A -> nat -> option A].
*)

(* our cons here is just a wrapper around Coq's cons *)

Definition cons : forall {A}, A -> list A -> list A :=
 fun {A} a l => List.cons a l.
(* or fun {A} a m => a::l *)
(* or just directly @List.cons. *)
(* complexity : O(1) *)

Definition uncons : forall {A}, list A -> option (A * list A) :=
 fun {A} l =>
   match l with
   | [] => None
   | x::l => Some (x,l)
   end.
(* complexity : O(1) *)

Definition nth : forall {A}, list A -> nat -> option A :=
 List.nth_error.
(* worst complexity : O(n) where n is the size of the list *)


(** Finish the previous module : *)

End RegularList.


(* *)
(** **** Exercise 16 : Implementation via b-lists 
(a.k.a binary lists)

We will now devise a data-structure where both [cons], 
[uncons] and [nth] operations will all be logarithmic 
(at worst).

We call _b-list_ a (regular) list of perfect binary 
trees with the following properties : the datas are 
stored at the leaves of the trees, and the sizes of 
the trees are strictly increasing when going through 
the external list from left to right. The elements of 
a b-list are the elements of the leftmost tree (from 
left to right) then the elements of the next tree, 
until the elements of the rightmost tree.

- Start again a module dedicated to this exercise : 
 [Module BList.]
*)
Module BList.

(** - Define a Coq type [blist : Type -> Type] corresponding 
 to b-list, i.e. list of binary trees with data at the 
 leaves. No need to enforce in [blist] the other 
 constraints (trees that are all perfect and are of 
 strictly increasing sizes). But you should always 
 maintain these invariants when programming with [blist]. 
 And if you wish you may write later boolean tests 
 checking whether a particular [blist] fulfills these 
 invariants. 
*)


Inductive tree A :=
| leaf : A -> tree A
| node : tree A -> tree A -> tree A.

Check node (leaf 1) (leaf 2).

Check node (node (leaf 1) (leaf 2)) (leaf 3). (* Some unbalanced tree we
  want to avoid *)

Fixpoint depth {A} (t:tree A) :=
(* optimistic depth, supposing the tree to be perfect *)
match t with
| leaf _ => 0
| node t' _ => S (depth t')
end.

Definition blist (A:Type) : Type := list (tree A).
(* or later a version with the size or depth of tree coming along each
   trees, to avoid recomputing this information all the time. *)


(** - Write some examples of small b-lists. In particular 
 how is encoded the empty b-list ? Could we have two 
 b-lists of different shape while containing the same 
 elements ?
*)

Definition empty_blist {A} : blist A := [].

Definition size1_blist {A} (a:A) : blist A := [leaf a].
Definition size2_blist {A} (a b:A) : blist A := [node (leaf a) (leaf b)].
(* etc. The size of the trees are the binary decomposition of the total
   number of elements in the blist. Eg. 7 = 1 + 2 + 4 *)



(** - (Optional) Adjust your [blist] definition in such a 
 way that retrieving the sizes (or depth) of a tree 
 inside a [blist] could be done without revisiting the 
 whole tree.
*)

(** - On this [blist] type, implement operations [cons], 
 [uncons] and [nth]. Check that all these operations 
 are logarithmic (if you did the last suggested step).
*)

(* Important auxiliary function : putting a tree on the left of a bl. *)
(* invariant : depth t <= depth of leftmost tree in the bl *)
Fixpoint constree {A} (t:tree A) (bl:blist A) : blist A :=
 match bl with
 | [] => [t]
 | t'::bl' =>
   if depth t =? depth t' then constree (node t t') bl'
   else t :: bl
 end.

Definition cons {A} (a:A) bl := constree (leaf a) bl.

Compute cons 2 (cons 3 (size2_blist 5 7)).

(* digression : from regular list to blist and back *)
Fixpoint list_to_blist {A} (l:list A) : blist A :=
 match l with
 | [] => empty_blist
 | x::l => cons x (list_to_blist l)
 end.

Compute list_to_blist [1;2;3;4;5;6;7].

Fixpoint tree_to_list {A} (t:tree A) : list A :=
 match t with
 | leaf a => [a]
 | node t t' => tree_to_list t ++ tree_to_list t'
 end.

Fixpoint blist_to_list {A} (bl:blist A) :=
 match bl with
 | [] => []
 | t::bl => tree_to_list t ++ blist_to_list bl
 end.
(* end of digression *)

(* depth t < depth of every tree in acc *)
Fixpoint unconstree {A} (t:tree A) (acc : blist A) : A * blist A :=
 match t with
 | leaf a => (a, acc)
 | node t t' => unconstree t (t'::acc)
 end.

Definition uncons {A} (bl : blist A) : option (A * blist A) :=
 match bl with
 | [] => None
 | t::bl => Some (unconstree t bl)
 end.
(* O(lg(n)) *)

Check Nat.log2.
Compute (2^4).
Compute Nat.log2 16.

(* invariant : n < size t (which is 2^depth t) *)
Fixpoint nthtree {A} (t:tree A) n : option A :=
 match t with
 | leaf a => Some a (* normally here n must be 0 *)
 | node t1 t2 =>
   let size_t1 := 2^depth t1 in
   if n <? size_t1 then nthtree t1 n
   else nthtree t2 (n-size_t1)
 end.

Fixpoint nth {A} (bl: blist A) n : option A :=
 match bl with
 | [] => None
 | t::bl' =>
   let size_t := 2^depth t in
   if n <? size_t then nthtree t n
   else nth bl' (n-size_t)
 end.
(* O(lg(n)) *)

Compute nth (list_to_blist [1;2;3;4;5;6;7]) 6.


(** - Finish the current module : [End BList.]*)

End BList.


Module BListWithSize.
Import BList. (* for BList.tree *)

Definition blist (A:Type) : Type := list (nat * tree A).
(* nat is here the size of the tree along it.
   Could be the size if you prefer. *)

(* Let's adapt all the previous code to avoid recomputing any depth *)

(* Important auxiliary function : putting a tree on the left of a bl. *)
(* invariant : depth t <= depth of leftmost tree in the bl *)
Fixpoint constree {A} (t:tree A) (depth:nat) (bl:blist A) : blist A :=
 match bl with
 | [] => [(depth,t)]
 | (depth',t')::bl' =>
   if depth =? depth' then constree (node t t') (S depth) bl'
   else (depth,t) :: bl
 end.

Definition cons {A} (a:A) bl := constree (leaf a) 0 bl.

Compute cons 2 (cons 3 (cons 5 (cons 7 []))).

(* digression : from regular list to blist and back *)
Fixpoint list_to_blist {A} (l:list A) : blist A :=
 match l with
 | [] => []
 | x::l => cons x (list_to_blist l)
 end.

Compute list_to_blist [1;2;3;4;5;6;7].

Fixpoint blist_to_list {A} (bl:blist A) :=
 match bl with
 | [] => []
 | (_,t)::bl => tree_to_list t ++ blist_to_list bl
 end.
(* end of digression *)

(* depth t < depth of ervery tree in acc *)
Fixpoint unconstree {A} (t:tree A) depth (acc : blist A) : A * blist A :=
 match t with
 | leaf a => (a, acc)
 | node t t' =>
   let depth' := depth-1 in unconstree t depth' ((depth',t')::acc)
 end.

Definition uncons {A} (bl : blist A) : option (A * blist A) :=
 match bl with
 | [] => None
 | (depth,t)::bl => Some (unconstree t depth bl)
 end.
(* O(lg(n)) *)

(* invariant : n < size (which is 2^depth t) *)
Fixpoint nthtree {A} (t:tree A) n size : option A :=
 match t with
 | leaf a => Some a (* normally here n must be 0 *)
 | node t1 t2 =>
   let size' := Nat.div2 size in
   if n <? size' then nthtree t1 n size'
   else nthtree t2 (n-size') size'
 end.

Fixpoint nth {A} (bl: blist A) n : option A :=
 match bl with
 | [] => None
 | (depth,t)::bl' =>
   let size_t := 2^depth in
   if n <? size_t then nthtree t n size_t
   else nth bl' (n-size_t)
 end.
(* O(lg(n)) *)

Compute nth (list_to_blist [1;2;3;4;5;6;7]) 6.

End BListWithSize.


(** This b-list structure shows that a fast random access 
in a list-like structure is indeed possible. But this 
comes here at an extra cost : the operations "on the 
left" ([cons] and [uncons]) have a complexity that is 
not constant anymore. We'll see now how to get the best 
of the two worlds. But first, some arithmetical 
interlude : in the same way the b-lists were closely 
related with the binary decomposition of number, here 
comes an alternative decomposition that may help us in 
exercise 18.
*)

(* *)
(** **** Exercise 17 : Skew binary number system

We call skew binary decomposition (or sb-decomposition) of a number 
its decomposition as sum of numbers of the form 2^k -1 with k>0. 
Moreover all the terms in this sum must be differents, except 

possibly the two smallest ones.

- Write a [decomp] function computing a sb-decomposition for any 
 natural number. Could we have different ordered sb-decompositions
 of the same number ?

- Write two functions [next] and [pred] that both take the 
 sb-decomposition of a number, and compute the sb-decomposition 
 of its successor (resp. precedessor), without trying to convert 
 back the number in a more standard representation, and moreover 
 without using any recursivity (no [Fixpoint]) apart from a 
 possible use of [Nat.eqb].

For the last section, we admit that the sb-decomposition of a number 
[n] is a sum whose number of terms is logarithmic in [n].
*)

(** left to the reader *)


(* *)
(** **** Exercise 18 : Implementation via sb-lists (skew binary lists)

- Based on all previous questions, propose a data-structure of *skew 
 binary lists* for which [cons] and [uncons] have constant complexity 
 while [nth] is logarithmic.

- Compare these sb-lists with the usual Coq lists : what reason may 
 explain that sb-lists are not used universally instead of usual lists ?

*)

(** left to the reader *)


(*
**** Exercise 19: Possible extensions

- For b-lists and sb-lists, code a function [drop] of logarithmic 
 complexity such that [drop k l] returns the list [l] except its 
 first [k] elements.

- For b-lists and sb-lists, code a function [update_nth] such that 
 [update_nth l n a] is either [Some l'] when [l'] is [l] except for 
 [a] at position [n], or [None] when [n] is not a legal position in [l].

- Use a binary representation of numbers instead of [nat], and compute 
 the complexity of all operations b-list and sb-list operations when 
 taking in account the cost of the arithmetical sub-operations.
*)


(** left to the reader *)


(** **** Exercice 20 : programming with vectors *)



Inductive vect (A:Type) : nat -> Type :=
 | Vnil : vect A 0
 | Vcons n : A -> vect A n -> vect A (S n).

Arguments Vnil {A}.
Arguments Vcons {A} {n}.

Require Import List.
Import ListNotations.

Fixpoint v2l {A} {n} (v : vect A n) : list A :=
  match v with
  | Vnil => []
  | Vcons x v => x::(v2l v)
  end.

Fixpoint l2v {A} (l: list A) : vect A (length l) :=
  match l with
  | [] => Vnil
  | x :: l' => Vcons x (l2v l')
  end.

Definition length' {A} {n} (v: vect A n) : nat := n.


Definition Vhead {A} {n} (v:vect A (S n)) : A :=
 match v with
 | Vcons x _ => x
 end.


(**

- Define a tail function on vectors:
 [Vtail
     : forall (A : Type) (n : nat), vect A (S n) -> vect A n]. 
*)

Definition Vtail {A} {n} (v:vect A (S n)) : vect A n :=
 match v with
 | Vcons _ w => w
 end.

Check @Vtail.

(** - Define an inverse to the cons function on vectors which takes 
a non-empty vectors and returns the pair of its head and tail:
[Vuncons
     : forall (A : Type) (n : nat), vect A (S n) -> A * vect A n]
*)

Definition Vuncons {A} {n : nat} (v : vect A (S n)) : A * vect A n := (Vhead v, Vtail v).

Check @Vuncons. 

(* From Coq Require Vector.
Import Vector.VectorNotations.
*)



(* 
 Require Import Arith_base.
Require Vectors.Fin.
Import EqNotations.
Local Open Scope nat_scope.
*)



(** - Define a map function on vectors: 
 [Vmap: forall (A B : Type) (n : nat), 
 vect A n -> (A -> B) -> vect B n]
*)


Fixpoint Vmap {A B} {n} (v: vect A n) (f: A -> B) : (vect B n) :=
match v with 
| Vnil => Vnil
| Vcons x v' => Vcons (f x) (Vmap v' f)
end. 

Check Vmap.

Fixpoint Vmap' {A B n} (v: vect A n) (f: A -> B) : (vect B n) :=
match v with 
| Vnil => Vnil
| @Vcons _ m x v' => @Vcons B m (f x) (Vmap' v' f)
end. 

Check Vmap.

Check Vmap'.



(** - Define a _zip_ function on vectors, that takes two vectors 
 of the same length and builds the vectors of the pairs:
 [Vzip : forall (A B : Type) (n : nat),
       vect A n -> vect B n -> vect (A * B) n]
*)

(** comment on the fact that trying to define Vzip as: 

Fixpoint Vzip {A B: Type} {n: nat} (v: vect A n) (w : vect B n) : 
(vect (A * B)  n) := ...

is problematic. *)


Fixpoint Vzip {A B: Type} {n: nat} (v: vect A n) : 
(vect B n) -> (vect (A * B)  n) :=
match v in vect _ n return vect B n -> (vect (A * B) n) with 
| Vnil => (fun _ => @Vnil (A * B))
| Vcons a v' => (fun w => Vcons (a,  Vhead w) (Vzip v' (Vtail w)))
end.

Check @Vzip.

Fixpoint Vzip' {A B: Type} {n: nat} : 
(vect A n) -> (vect B n) -> (vect (A * B)  n) :=
match n  return vect A n -> vect B n -> (vect (A * B) n) with 
| 0 => (fun _ => fun _ => @Vnil (A * B))
| S m  => (fun v => fun w => Vcons (Vhead v,  Vhead w) (Vzip' (Vtail v) (Vtail w)))
end.

Check @Vzip'.



(** - Define an append function on vectors: 
[Vappend : forall (A : Type) (n p : nat),
       vect A n -> vect A p -> vect A (n + p)] *)

Fixpoint Vappend {A}{n}{p} (v:vect A n) (w:vect A p):vect A (n+p) :=
  match v with
  | Vnil => w
  | Vcons a v' => Vcons a (Vappend v' w)
  end.

Check @Vappend.




(** - Define a split function that splits a vectors in a pair of two vectors, 
 the first one containing a prefix of a specified size and the second 
containing the rest of the input vector:
[Vsplitat
     : forall (A : Type) (l r : nat),
       vect A (l + r) -> vect A l * vect A r]
*)

Fixpoint Vsplitat {A} (l : nat) {r : nat} :
  vect A (l + r) -> vect A l * vect A r :=
  match l return vect A (l + r) -> vect A l * vect A r with
  | 0 => fun v => (Vnil, v)
  | S l' => fun v =>
    let (v1, v2) := Vsplitat l' (Vtail v) in
    (Vcons (Vhead v) v1, v2)
  end.

Check @Vsplitat.



(** **** Exercise 21 : perfect binary trees, dependent case

Redo Exercise 16, but this time use a dependent type for trees 
in `blist`, to ensure that all trees are necessarily perfect.

*)

Module BList_DepType.

Inductive fulltree (A:Type) : nat -> Type :=
| FLeaf : A -> fulltree A 0
| FNode n : fulltree A n -> fulltree A n -> fulltree A (S n).

Arguments FLeaf {A}.
Arguments FNode {A} {n}.

Definition blist (A:Type) := list { n & fulltree A n }.

Definition empty_blist {A} : blist A := [].

Definition size1_blist {A} (a:A) : blist A := [existT _ _ (FLeaf a)].
Definition size2_blist {A} (a b:A) : blist A :=
 [existT _ _ (FNode (FLeaf a) (FLeaf b))].

(* A comparison returning (dis)equality proofs in addition to the
   yes/no answer *)
Check Nat.eq_dec.
(* For instance :
   match Nat.eq_dec n m with
        | left p => (* here (p : n=m) *) ...
        | right _ => (* here n <> m *) ...
        end
*)

(* Just like Vcast (see seance 4) *)
Definition treecast {A} {n} {m} (v: fulltree A n)(h : n = m) : fulltree A m :=
  match h with
  | eq_refl => v
  end.

(* Important auxiliary function : putting a tree on the left of a bl. *)
(* invariant : depth t <= depth of leftmost tree in the bl *)
Fixpoint constree {A} {n:nat} (t:fulltree A n) (bl:blist A) : blist A :=
 match bl with
 | [] => [existT _ n t]
 | (existT _ m t')::bl' =>
   match Nat.eq_dec n m with
   | left p => constree (FNode (treecast t p) t') bl'
   | right _ => (existT _ n t) :: bl
   end
 end.

Definition cons {A} (a:A) bl := constree (FLeaf a) bl.

Compute cons 2 (cons 3 (size2_blist 5 7)).
 (* Unlike with Vcast in seance 4, this computes ok since Nat.eq_dec
    returns a proof that isn't opaque... *)

(* digression : from regular list to blist and back *)
Fixpoint list_to_blist {A} (l:list A) : blist A :=
 match l with
 | [] => empty_blist
 | x::l => cons x (list_to_blist l)
 end.

Compute list_to_blist [1;2;3;4;5;6;7].

Fixpoint tree_to_list {A} {n} (t:fulltree A n) : list A :=
 match t with
 | FLeaf a => [a]
 | FNode t t' => tree_to_list t ++ tree_to_list t'
 end.

Fixpoint blist_to_list {A} (bl:blist A) :=
 match bl with
 | [] => []
 | (existT _ _ t)::bl => tree_to_list t ++ blist_to_list bl
 end.
(* end of digression *)

(* depth t < depth of every tree in acc *)
Fixpoint unconstree {A} {n} (t:fulltree A n) (acc : blist A) : A * blist A :=
 match t with
 | FLeaf a => (a, acc)
 | FNode t t' => unconstree t ((existT _ _ t')::acc)
 end.

Definition uncons {A} (bl : blist A) : option (A * blist A) :=
 match bl with
 | [] => None
 | (existT _ _ t)::bl => Some (unconstree t bl)
 end.
(* O(lg(n)) *)

(* invariant : n < size t (which is 2^depth t) *)
Fixpoint nthtree {A} {d} (t:fulltree A d) n : option A :=
 match t with
 | FLeaf a => Some a (* normally here n must be 0 *)
 | FNode t1 t2 =>
   let size_t1 := 2^(pred d) in
   if n <? size_t1 then nthtree t1 n
   else nthtree t2 (n-size_t1)
 end.

Fixpoint nth {A} (bl: blist A) n : option A :=
 match bl with
 | [] => None
 | (existT _ d t)::bl' =>
   let size_t := 2^d in
   if n <? size_t then nthtree t n
   else nth bl' (n-size_t)
 end.
(* O(lg(n)) *)

Compute nth (list_to_blist [1;2;3;4;5;6;7]) 6.

End BList_DepType.



(****************************************************************)
(****************************************************************)
(****************************************************************)
(****************************************************************)
(****************************************************************)
(****************************************************************)
(****************************************************************)
(****************************************************************)




