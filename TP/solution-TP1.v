(** * Solution1 : Solution to TP1, Introduction Functional <br /> Programming in Coq **)

(** * Practice

** Functions and only that

**** Exercise 1 : function composition

Define a function [compose : forall A B C, (B->C)->(A->B)->(A->C)]. 
Test it with functions [S] and [pred] on natural numbers (type [nat]).
*)

Section composition.
Variables A B C: Type.

Definition compose1 (f:B->C)(g:A->B)(x: A) := f (g x).

Definition compose2 := fun (f:B->C)(g:A->B)(x: A) => f (g x).

Definition compose3 (f:B->C)(g:A->B) := fun x => f (g x).

Definition compose4 := fun (f:B->C) g (x: A) => f (g x).

Definition compose5 := fun f (g:A -> B) x => f (g x):C.

Check compose1.
Check compose2.
Check compose3.
Check compose4.
Check compose5.

End composition.

Check compose1.
Check compose2.
Check compose3.
Check compose4.
Check compose5.

Definition compose6 (A B C: Type) (f:B->C)(g:A->B) (x: A):= f (g x).

Check compose6.

Compute compose6 nat nat nat S pred 7.
Compute compose6 nat nat nat S pred 0.
Compute compose6 nat nat nat pred S 0.

Definition composenat := compose6 nat nat nat.

Check composenat.

Compute composenat S pred 7.
Compute composenat S pred 0.
Compute composenat pred S 0.

(** if one describes precisely the type of [compose]
it is not needed to provide the type of each arguments 
and even A B C can be left implicit, using a generic 
[_] pattern: *)

Definition compose7 : forall A B C, (B->C)->(A->B)->A->C :=
fun _ _ _ f g x => f (g x).

Check compose7.


Definition compose8 {A B C} (f:B->C)(g:A->B) := fun x => f (g x).


(** Here, the arguments A B C of [compose] are treated 
implicitly, they are implicit arguments of the function
which are not required to be passed to the function as 
being inferred from the other arguments (here, [f], [g] 
and [x].. 
*)

Check compose8.

Compute compose8 S pred 7.
Compute compose8 S pred 0.
Compute compose8 pred S 0.

(** Still, it is possible to deactivate the implicit 
argument to specialise a function as for the composition 
on functions over nat: *)

Definition composenat' := @compose7 nat nat nat.

Check composenat'.

(** **** Exercise 2 : Boolean ersatz

Define (without using [bool] nor any other inductive type):

- a type [mybool : Type];
- two constants [mytrue] and [myfalse] of type [mybool];
- a function [myif : forall A, mybool -> A -> A -> A] such that 
 [myif mytrue x y] computes to [x] and [myif myfalse x y] computes to [y].

*)

(* *)

(** Similarly to the type [forall X, X -> X], inhabited by 
only one value, the identity function and which can 
therefore serve as a [unit] type, one can define a type with two 
elements which is known as the church encoding of booleans: *)

Definition mybool := forall X, X->X->X.

(** That type is indeed inhabited by two functions only, 
the first and second projections: *)

Definition mytrue : mybool := fun _ x y => x.
Definition myfalse : mybool := fun _ x y => y.

(** On can therefore define a conditional branching based 
on the operation behaviour of [mytrue] and [myfalse]: 
we want [myif b x y] to return [x] if 
[b] is [mytrue] (ie the first projection) and
to return [y] if 
[b] is [myfalse] (ie the second projection). 
In both cases, [myif b x y] shall behave like [b x y]... 
from which we infer the definition: *)


Definition myif : forall {Y}, mybool -> Y -> Y -> Y :=
 fun _ b x y => b _ x y.

Compute myif mytrue 0 1.
Compute myif myfalse 0 1.



(** **** Exercise 3 : Church numerals

Encode in Coq the Church numerals, for which the number 
<<n>> is represented by <<λf.λx.(f (f (... (f x))))>> where 
<<f>> is applied <<n>> times.

More precisely, define (without using [nat] nor any other inductive type):

 - a type [church : Type]
 - two constant [zero] and [one] of type [church]
 - a function [church_succ] of type [church->church]
 - two functions [church_plus] and [church_mult] of type [church->church->church]
 - a function [church_power]
 - a test [church_iszero]

Also define two functions [nat2church : nat -> church] and [church2nat : church -> nat] 

*)

(* *)

(** Let us first analyze the type of Church numerals:
the statement of the exercise tells us that church is 
a function of two arguments, [f] and [x], which iterates 
[f] a certain number of time over [x]. 

Therefore [f] shall have a type of the form [X -> Y] 
and necessarily [X] must be the type of [x]. Moreover, 
since [f] can be iterated, the type of its codomain shall 
be the same as the type of its domain: [X]=[Y]. 
Moreover, we want this type to apply on any pair of arguments 
the first of which is iterable on the second, and we thus 
introduce a universal quantification (dependent product type)
ending up with [forall X, (X->X)->(X->X)] as a type of 
Church numerals.

*)

Definition church := forall X, (X->X)->(X->X).

(** in defining [zero, one, ...], it is not 
needed to give the type of each argument (and neither must 
we provide a name for the type argument [X]) 
when the type of the constant is given : *)

Definition zero : church := fun _ f x => x.
Definition one : church := fun _ f x => f x.

(** in fact there is another candidates for one: *)

Definition onebis : church := fun _ f => f.
Definition two : church := fun _ f x => f (f x).

Definition succ : church -> church :=
 fun n => fun _ f x => n _ f (f x).

Compute succ one.

Definition church2nat (n:church) := n _ S 0.

Compute church2nat zero.
Compute church2nat one.
Compute church2nat onebis.
Compute church2nat two.

Fixpoint nat2church (n:nat) :=
 match n with
 | O => zero
 | S m => succ (nat2church m)
 end.


Compute nat2church 3.
Compute church2nat (nat2church 100).

(** In the following, we will use the ability of Coq 
to not require that we actually provide the actual 
type when it can be inferred to lighten the definitions.

But to be fully informative and in order to understand 
what happens with church_power, we provide below the 
definition of those functions providing explicitly the 
type instanciations: *)

Definition church_plus (n m:church) : church :=
  fun _ f x => n _ f (m _ f x).


Compute church_plus one two.
Compute church2nat (church_plus (nat2church 13) (nat2church 10)).

Definition church_mult (n m:church) : church :=
 fun _ f =>  n _ (m _ f).

Compute church_mult one two.
Compute church2nat (church_mult (nat2church 13) (nat2church 10)).

Definition church_pow (n m:church) : church :=
 fun _ =>  m _ (n _).


Compute church_pow two two.
Compute church2nat (church_pow (nat2church 2) (nat2church 5)).

(** To compute whether n is equal to zero of not, we 
pass two arguments to n. The first one will be iterated 
n times on the second one. 
- Therefore, if n is zero, the first function is not 
iterated and the second argument is returned directly, 
this second argument should be [true]. 
- Otherwise, if n is not zero, the function will be 
iterated at least one and we would like is_zero n to 
return false, therefore it is sufficient that the 
first argument of n is the the constant function 
always returning zero, independently of its argument: 
[fun _ => false]. *)


Definition is_zero (n : church) : bool :=
  n _ (fun _ => false) true.

Compute is_zero zero.
Compute is_zero one.

(** Explicit typing of the above functions: *)

Definition church_plus_explicit (n m:church) : church :=
  fun X f x => n X f (m X f x).

Definition church_mult_explicit (n m:church) : church :=
 fun X f =>  n X (m X f).

Definition church_pow_explicit (n m:church) : church :=
 fun X =>  m (X->X) (n X).

Definition is_zero_explicit (n : church) : bool :=
  n bool (fun (b:bool) => false) true.





(** ** Base types

**** Exercise 4 : Booleans

- Write a function [checktauto : (bool->bool)->bool] which tests whether 
 a Boolean unary function always answers [true] (difficulty: easy).
- Same for [checktauto2] and [checktauto3] for Boolean functions expecting 2, 
 then 3 arguments. This can be done by enumerating all cases, but there is a 
 clever way to proceed (for instance by re-using [checktauto] 
 (difficulty: medium).
- Check whether [fun a b c => a || b || c || negb (a && b) || negb (a && c)] is 
 a tautology (difficulty: easy).
 Note : the command [Open Scope bool_scope.] activates notations [||] and [&&] 
 (respectively for functions [orb] and [andb]).
- Define some functions behaving like Coq standard functions [negb] and [orb] 
 and [andb]. (difficulty: easy)
*)

(* *)

Open Scope bool_scope.

Definition checktauto f := f true && f false.
Definition checktauto2 f := checktauto (compose checktauto f).
Definition checktauto3 f := checktauto (compose checktauto2 f).

Compute checktauto3
        (fun a b c => a || b || c || negb (a && b) || negb (a && c)).
Compute checktauto3 (fun a b c => a || b || c).

(** A revoir lors du cours sur les "types dépendants" :
    une généralisation de checktauto aux fonctions booléennes
    à n arguments. P.ex. nbool 2 = bool -> bool -> bool
    Et nchecktauto 2 se comportera comme le checktauto2 précédent. *)

Fixpoint nbool n : Type :=
 match n with
 | 0 => bool
 | S n => bool -> nbool n
 end.

Fixpoint nchecktauto n : nbool n -> bool :=
 match n with
 | 0 => fun b => b
 | S n => fun f => nchecktauto n (f true) && nchecktauto n (f false)
 end.

Compute nchecktauto 3
        (fun a b c => a || b || c || negb (a && b) || negb (a && c)).
Compute nchecktauto 3 (fun a b c => a || b || c).



(** **** Exercise 5 : usual functions on natural numbers.

Define the following functions of type [nat  -> nat -> nat] 
(without using the ones of Coq standard library of course!):

- [addition] (difficulty: easy)
- [multiplication] (difficulty: easy)
- [subtraction] (difficulty: medium)
- [power] (difficulty: medium)
- [modulo] such that [modulo a b] computes a modulo b 
 (difficulty: medium)
- [gcd] (difficulty: hard)

Ackermann-Péter (difficulty: hard):
We recall that the Ackermann-Péter function, AP, is defined as: 
- AP(0,n) = n+1
- AP(m+1, 0) = AP(m, 1)
- AP(m+1, n+1) = AP(m, AP(m+1, n)).

Try defining [AP], of type [nat -> nat -> nat], in the 
most natural way, based on its definition. 
What problem do you encounter?

What possible workaround can you imagine?

*)

Require Import Arith.

Fixpoint add n m :=
 match n with
 | 0 => m
 | S n => S (add n m)
 end.

Compute add 3 7.

Fixpoint mul n m :=
 match n with
 | 0 => 0
 | S n => add m (mul n m)
 end.

Compute mul 3 7.

Fixpoint sub n m :=
 match n, m with
 | _,0 => n
 | 0,_ => n
 | S n, S m => sub n m
 end.

Compute sub 7 3.
Compute sub 3 7.

Fixpoint fact n :=
 match n with
 | 0 => 1
 | S m => mul n (fact m)
 end.

Compute fact 7.
Fail Compute fact 8.


Fixpoint pow a b :=
 match b with
 | 0 => 1
 | S b => mul a (pow a b)
 end.

Compute pow 2 10.


Fixpoint modulo_loop a b n :=
 match n with
 | 0 => 0
 | S n =>
   if a <? b then a
   else modulo_loop (sub a b) b n
 end.

Definition modulo a b := modulo_loop a b a.

Compute modulo 10 3.
Compute modulo 3 0.

Fixpoint gcd_loop a b n :=
  match n with
  | 0 => 0
  | S n =>
    if b =? 0 then a
    else gcd_loop b (modulo a b) n
  end.

Definition gcd a b := gcd_loop a b (S b).

Compute gcd 17 23.
Compute gcd 23 17.
Compute gcd 12 9.
Compute gcd 9 12.
Compute gcd 10 1.
Compute gcd 1 10.
Compute gcd 0 7.
Compute gcd 7 0.


Fail Fixpoint AP (m n: nat) :=
match (m,n) with 
| (0 , _) => n+1
| (S m', 0) => AP m 1
| (S m', S n') => AP m' (AP m n)
end.

Fixpoint AP (m: nat) : nat -> nat :=
match m with 
| 0 => (fun x => x+1)
| S m' => 
fix APaux (n: nat) := match n with 
| O => AP m' 1
| S n' => AP m' (APaux n')
end
end.

(* *)
